﻿module Automata

type astate<'A when 'A : comparison> =
        | SState of 'A
        | MState of Set<astate<'A>>

        override this.ToString() = 
            match this with
                | SState(a) -> a.ToString()
                | MState(ss) -> let sbuilder = new System.Text.StringBuilder()
                                Set.iter (fun s -> Printf.bprintf sbuilder "%s" (s.ToString())) ss
                                sbuilder.ToString()

let makeState a = SState(a)

type automata<'A, 'B when 'A : comparison and 'B : comparison> = 
    {states : Set<astate<'A>>; initial : astate<'A>; finals : Set<astate<'A>>; sigma : Set<'B>; trans : Map<astate<'A>,Map<'B option, Set<astate<'A>>>>}

let empty id =
    let istate = SState(id)
    let ss = Set.singleton istate
    {states = ss;
     initial = istate;
     finals = Set.empty;
     sigma = Set.empty;
     trans = Map.empty}

let singleton id =
    let istate = SState(id)
    let ss = Set.singleton istate
    {states = ss;
     initial = istate;
     finals = ss;
     sigma = Set.empty;
     trans = Map.empty}

let addTrans s1 c s2 a = 
    if Set.contains s1 a.states then
        let ntran = match a.trans.TryFind s1 with
                     | None -> Map.empty.Add(c,Set.singleton s2)
                     | Some(m) -> match m.TryFind c with
                                  | None -> m.Add(c, Set.singleton s2)
                                  | Some(ss) -> m.Add(c, ss.Add s2)
        let ntrans = a.trans.Add(s1,ntran)
        {a with trans = ntrans; states = a.states.Add(s2); sigma = if c.IsSome then a.sigma.Add(c.Value) else a.sigma} 
    else
        failwith (Printf.sprintf "addTransition: no reachable source state: %s" (s1.ToString()))

let addTransitions lt a = 
    Seq.fold (fun a' (s1,c,s2) -> addTrans s1 c s2 a') a lt

let addFinals s a = 
    if a.states.Contains s then
        {a with finals = a.finals.Add s}
    else
        failwith "addFinals: unknown state"


let toGraphViz a = 
    let sbuilder = new System.Text.StringBuilder()
    Printf.bprintf sbuilder "digraph {\nrankdir=LR;\tnode[shape=point]; _0_;\nnode[shape = doublecircle];" 
    a.finals |> Set.iter (fun s -> Printf.bprintf sbuilder "\"%s\";" (s.ToString()))
    Printf.bprintf sbuilder "node[shape = circle]\n_0_ -> \"%s\";\n" (a.initial.ToString())
    
    a.trans |> Map.iter (fun s1 m -> m |> Map.iter ( fun b ss -> let sb = if b.IsSome then b.Value.ToString() else "[t]"
                                                                 ss |> Set.iter (fun s2 -> Printf.bprintf sbuilder "\"%s\" -> \"%s\" [label = \"%s\"];\n" (s1.ToString()) (s2.ToString()) sb)
                                  )
                        )

    sbuilder.Append("}") |> ignore
    sbuilder.ToString()

let toGMC (name : string) a =
    let sbuilder = new System.Text.StringBuilder()
    Printf.bprintf sbuilder ".outputs %s\n.state graph\n" <| (name.Replace("_",""))

    a.trans |> Map.iter (fun s1 m -> m |> Map.iter ( fun b ss -> let sb = if b.IsSome then b.Value.ToString() else "tau"
                                                                 ss |> Set.iter (fun s2 -> Printf.bprintf sbuilder "%s %s %s\n" (s1.ToString()) sb (s2.ToString()))))
    Printf.bprintf sbuilder ".marking %s\n.end" (a.initial.ToString())
    sbuilder.ToString()

let private mjoin m1 m2 = Map.fold (fun m k v -> Map.add k v m) m1 m2

(*
let append a1 c a2 = 
    if (Set.intersect a1.states a2.states).IsEmpty then
        let deltaTrans = a1.finals |> Set.map (fun s -> (s, c, a2.initial)) |> Set.toSeq |> addTransitions
        {states = Set.union a1.states a2.states;
         initial = a1.initial;
         finals = a2.finals;
         sigma = Set.union a1.sigma a2.sigma;
         trans = mjoin a1.trans a2.trans} |> deltaTrans
    else
        failwith "append: automata have states in common"
*)


let appendL a1 cl a2 = 
    if (Set.intersect a1.states a2.states).IsEmpty then
        let deltaTrans = [for s in a1.finals do for c in cl do yield (s,c)]  |> List.map (fun (s,c) -> (s, c, a2.initial)) |>  addTransitions
        {states = Set.union a1.states a2.states;
         initial = a1.initial;
         finals = a2.finals;
         sigma = Set.union a1.sigma a2.sigma;
         trans = mjoin a1.trans a2.trans} |> deltaTrans
    else
        failwith "append: automata have states in common"

let append a1 c a2 = appendL a1 [c] a2            

let loop a = 
    let deltaTrans = a.finals |> Set.map (fun s -> (s, None, a.initial)) |> Set.toSeq |> addTransitions
    {a with finals = Set.singleton a.initial} |> deltaTrans

let choice (s1,s2) a1  a2 = 
    let us = Set.union a1.states a2.states
    let ns = Set.ofList [s1; s2]
    if (Set.intersect us ns).IsEmpty then
        if (Set.intersect a1.states a2.states).IsEmpty then
            let deltaTrans = Set.union a1.finals a2.finals |> 
                             Set.map (fun s -> (s, None, s2)) |> 
                             Set.toSeq |> 
                             Seq.append [s1,None,a1.initial ; s1,None,a2.initial] |> 
                             addTransitions        
            {
                states = Set.union us ns;
                initial = s1;
                finals = Set.singleton s2;
                sigma = Set.union a1.sigma a2.sigma;
                trans = mjoin a1.trans a2.trans
            } |> deltaTrans
        else
            failwith "choice: automata have states in common"
    else
        failwith "choice: invalid start or final state"

let removeUnreachable a = 
    let getSuccessors s = match a.trans.TryFind s with
                            | None -> []
                            | Some(km) -> let taus = km.TryFind None |> Option.toList
                                          let cs = Set.map (fun c -> km.TryFind (Some(c)) |> Option.toList) a.sigma |> List.concat 
                                          List.append taus cs |>
                                          Set.unionMany |>
                                          Set.toList
        
    let rec loop stack visit = 
        match stack with
            | [] -> visit
            | h::t -> if Set.contains h visit then
                        loop t visit
                      else
                        let ns = getSuccessors h |> List.filter (fun s -> not(Set.contains s visit))
                        loop (List.append ns t) (visit.Add(h))
    let reachable = loop [a.initial] Set.empty
    {a with states = reachable; trans = Map.filter (fun k v -> Set.contains k reachable) a.trans; finals = Set.intersect reachable a.finals}

let shuffle a1 a2 =
    let sproduct S1 S2 = [for s1 in S1 do for s2 in S2 do yield MState(Set.ofList [s1 ; s2])] |> Set.ofList
    let sstates = sproduct a1.states a2.states
    let sinitial = MState((Set.singleton a1.initial).Add(a2.initial))
    let sfinals = sproduct a1.finals a2.finals
    let ssigma = Set.union a1.sigma a2.sigma
    let singleTransC c (s1, m1) (s2,m2) = 
        (match Map.tryFind c m1, Map.tryFind c m2 with
            | Some(ss1), Some(ss2) -> [(c, Set.ofList [for s1 in ss1 do for s2 in ss2 do yield MState(Set.ofList [s1;s2])])]
            | Some(ss1), None -> [(c, Set.ofList [for s1 in ss1 do yield MState(Set.ofList [s1;s2])])]
            | None, Some(ss2) -> [(c, Set.ofList[for s2 in ss2 do yield  MState(Set.ofList [s1;s2])])]
            | _ -> []) |> Map.ofList
    let singleTransS s1 s2 = 
        match Map.tryFind s1 a1.trans, Map.tryFind s2 a2.trans with
            | Some(map1),Some(map2) -> let m1 = Set.fold (fun m c -> mjoin m (singleTransC (Some c) (s1, map1) (s2,map2))) Map.empty ssigma
                                       let m2 = singleTransC None (s1,map1) (s2,map2)
                                       mjoin m1 m2
            | Some(map1), None -> Map.map (fun c ss -> Set.map (fun s -> MState(Set.ofList [s;s2])) ss) map1
            | None, Some(map2) -> Map.map (fun c ss -> Set.map (fun s -> MState(Set.ofList [s1;s])) ss) map2
            | _ -> Map.empty
        
    let strans = Map.ofList [for s1 in a1.states do for s2 in a2.states do yield (MState(Set.ofList [s1; s2]), singleTransS s1 s2)]
    {states = sstates; initial = sinitial; finals = sfinals; sigma = ssigma; trans = strans}  |> removeUnreachable

let merge a1 a2 =
    let sproduct S1 S2 = [for s1 in S1 do for s2 in S2 do yield MState(Set.ofList [s1 ; s2])] |> Set.ofList
    let sstates = sproduct a1.states a2.states
    let sinitial = MState((Set.singleton a1.initial).Add(a2.initial))
    let sfinals = sproduct a1.finals a2.finals
    let ssigma = Set.union a1.sigma a2.sigma
    let singleTransC c (s1, m1) (s2,m2) = 
        let trans1 = Map.tryFind c m1 |> Option.map (Set.map (fun s -> MState <| Set.ofList [s;s2])) |> Option.toList |> Set.unionMany
        let trans2 = Map.tryFind c m2 |> Option.map (Set.map (fun s -> MState <| Set.ofList [s1;s])) |> Option.toList |> Set.unionMany
        let trans = Set.union trans1 trans2
        [ (c, trans) ] |> Map.ofList
    let singleTransS s1 s2 = 
        match Map.tryFind s1 a1.trans, Map.tryFind s2 a2.trans with
            | Some(map1),Some(map2) -> let m1 = Set.fold (fun m c -> mjoin m (singleTransC (Some c) (s1, map1) (s2,map2))) Map.empty ssigma
                                       let m2 = singleTransC None (s1,map1) (s2,map2)
                                       mjoin m1 m2
            | Some(map1), None -> Map.map (fun c ss -> Set.map (fun s -> MState(Set.ofList [s;s2])) ss) map1
            | None, Some(map2) -> Map.map (fun c ss -> Set.map (fun s -> MState(Set.ofList [s1;s])) ss) map2
            | _ -> Map.empty
        
    let strans = Map.ofList [for s1 in a1.states do for s2 in a2.states do yield (MState(Set.ofList [s1; s2]), singleTransS s1 s2)]
    {states = sstates; initial = sinitial; finals = sfinals; sigma = ssigma; trans = strans}  |> removeUnreachable

let e_closureSet states a = 
    let rec loop acc stack = 
        match stack with
            | [] -> acc
            | h :: t -> match a.trans.TryFind h with
                            | None -> loop acc t
                            | Some(m) -> match m.TryFind None with
                                           | None -> loop acc t
                                           | Some(l) -> let ls = Set.filter (fun s -> not(Set.contains s acc)) l 
                                                        loop (Set.union acc ls) ((Set.toList ls) @ t)
    loop states (Set.toList states)

let e_closure s a = 
    e_closureSet (Set.singleton s) a

let removeTau a = 
    let dict = Set.fold (fun acc s -> Map.add s (e_closure s a) acc) Map.empty a.states
    let reachableFromSC fromS c = 
        Set.fold (fun acc s -> match a.trans.TryFind(s) with
                                | None -> acc
                                | Some(m) ->  m.TryFind(c) |> Option.toList |> Set.ofList |> Set.unionMany |> Set.union acc)
                 Set.empty 
                 fromS
        |>
        Set.map (fun s -> MState(dict.[s])) 
    let reachableFromS fromS = 
        Set.fold (fun acc c ->  let ss = reachableFromSC fromS (Some(c))
                                if ss.IsEmpty then acc else Map.add (Some(c)) ss acc) 
                 Map.empty a.sigma
        
    let ntrans = Set.fold (fun acc s -> Map.add (MState dict.[s]) (reachableFromS dict.[s]) acc) Map.empty a.states 
    let nstates = dict |> Map.toList |> List.map (snd >> MState) |> Set.ofList
    let nfinals = nstates |> Set.filter (fun (MState(ss)) -> not((Set.intersect ss a.finals).IsEmpty))
    {a with states = nstates; 
            initial = MState(dict.[a.initial]);
            trans = ntrans;
            finals = nfinals } |> removeUnreachable
    

let rename nextName a = 
    let newNames = Set.fold (fun m s -> Map.add s (SState(nextName())) m) Map.empty a.states //Set.fold (fun m s -> m.Add(s, SState(nextName()))) Map.empty a.states 
    let internalMap c ss = ss |> Set.map (fun s -> newNames.[s]) 
    let externalMap s cm = cm |> Map.map internalMap 
    {a with states = a.states |> Set.map (fun s -> newNames.[s]) ;
            trans = a.trans |> Map.toSeq |> Seq.map (fun (k,v) -> (newNames.[k], Map.map internalMap v)) |> Map.ofSeq ;
            initial = newNames.[a.initial];
            finals = a.finals |> Set.map (fun s -> newNames.[s])
     }

let toDfa a = 
    let goto ss c = 
       let nstates = Set.fold (fun acc s -> match a.trans.TryFind(s) with
                                             | None -> acc
                                             | Some(m) -> m.TryFind(c) |> Option.toList |> Set.ofList |> Set.unionMany |> Set.union acc)
                               Set.empty
                               ss
       e_closureSet nstates a
    let gotoM ss = 
        Set.fold (fun acc c -> let ss = goto ss (Some(c))
                               if ss.IsEmpty then acc else (Some c,ss)::acc)
                 List.empty
                 a.sigma
    let rec loop stack set trans = 
        match stack with
            | [] -> (set, trans)
            | h :: t -> let ls = gotoM h
                        let ns = List.map snd ls |> Set.ofList 
                        let ns' = Set.difference ns set |> Set.toList
                        let trans' = List.map (fun (c,s) -> (c, MState(s) |> Set.singleton )) ls |> Map.ofList
                        loop (List.append ns' t) (Set.union set ns) (Map.add (MState h) trans' trans) 
    let initial = e_closure a.initial a
    let (ss, tr) = loop [initial] (Set.ofList [initial]) Map.empty
    let states = Set.map MState ss
    let finals = Set.filter (fun s -> not (Set.intersect s a.finals).IsEmpty) ss |> Set.map MState
    {a with states = states; 
            finals = finals;
            trans = tr;
            initial = MState (initial)}
                

let minimize a = 
    let isFinal s = Set.contains s a.finals
    let statesSeq = seq {for s in a.states do for s' in a.states do if s < s' then yield (s,s')} //
    let distinct = Seq.fold (fun distinct (s,s') -> if ((isFinal s) && not(isFinal s')) || ((isFinal s') && not(isFinal s)) then
                                                      Set.add (s,s') distinct
                                                    else
                                                      distinct)
                            Set.empty statesSeq
    let rec loop distinct = 
        let getTarget s c = 
            let m = a.trans.TryFind(s)
            if m.IsSome then
                let s' = m.Value.TryFind (Some(c))
                if s'.IsSome then
                    Some(s'.Value |> Set.toList |> List.head)
                else
                    None
            else
                None
        let dist (p,q) c dist = 
            if not (Set.contains (p,q) dist) then
                match (getTarget p c, getTarget q c) with
                  | Some(p'), Some(q') -> if (Set.contains (p',q') dist) then Set.singleton (p,q) else Set.empty
                  | Some(_), None 
                  | None, Some(_) -> Set.singleton (p,q)
                  | _ -> Set.empty
            else
                Set.empty
        let deltaDistinct = 
            Seq.fold (fun delta sp -> Set.fold (fun delta' c -> Set.union delta' (dist sp c distinct) ) delta a.sigma)
                     Set.empty
                     statesSeq
        if deltaDistinct.IsEmpty then
            distinct
        else
            loop (Set.union distinct deltaDistinct)
    Set.difference (Set.ofSeq statesSeq) (loop distinct)
    // Calcolata la relazione derivare gli stati
    
    
let testAutomata () = 
    let automaton0 = 
        {states = Set.singleton(SState 0).Add(SState 1); initial = (SState 0); finals = Set.singleton(SState 1); sigma = Set.singleton 'a'; trans = Map.empty} |>
        addTransitions [(SState 0), Some('a'), (SState 1); (SState 1), Some('b'), (SState 1)]
    let automaton1 = 
        append (empty 2) (Some('b')) (empty 3)
    let automaton2 = 
        loop automaton1
    let automaton3 = 
        choice (SState(5),SState(6)) automaton0 automaton1
    let automaton4 = 
        shuffle automaton0 automaton1
    let automaton5 =
        shuffle automaton1 automaton3
    let automaton6 = 
        {states = Set.singleton(SState 0); initial = (SState 0); finals = Set.empty; sigma = Set.empty; trans = Map.empty} |>
        addTransitions [ SState(0), None, SState(1); 
                         SState(0), None, SState(3);
                         SState(1), Some('b'), SState(2);
                         SState(3), Some('a'), SState(4);
                         SState(2), None, SState(5);
                         SState(4), None, SState(5);
                         SState(5), None, SState(0);
                         SState(5), None, SState(6);
                         SState(6), Some('b'), SState(7);
                         SState(7), Some('c'), SState(8); 
                         SState(8), Some('d'), SState(9)] |>
        addFinals (SState(9))
    let automaton7 = 
        let counter = ref(-1)
        automaton6 |> toDfa |> rename (fun () -> counter := !counter + 1; !counter)
    
    let automaton8 =
        {states = Set.singleton(SState 0); initial = (SState 0); finals = Set.empty; sigma = Set.empty; trans = Map.empty} |>
        addTransitions [ SState(0), None, SState(1);
                         SState(1), Some('a'), SState(2);
                         SState(2), Some('b'), SState(3);
                         SState(3), None, SState(9);
                         SState(0), None, SState(4);
                         SState(4), Some('a'), SState(5);
                         SState(5), None, SState(4);
                         SState(5), None, SState(6);
                         SState(6), Some('b'), SState(7);
                         SState(7), None, SState(6);
                         SState(7), None, SState(8);
                         SState(8), None, SState(9);
                        ] |>
         addFinals (SState 9)
    //let automaton9 = toDfa automaton8
    let automaton9 = merge automaton0 automaton8

    //let automata = [automaton0; automaton1; automaton2 ; automaton3 ; automaton4 ; automaton5 ; automaton6]
    [automaton0 ; automaton8; automaton9] |> Seq.iter (fun a -> printfn "**********\n%s\n********" (a |> toGraphViz))
    
    //printfn "********\n%s\n******\n" (automaton7 |> toGraphViz)
    //printfn "********\n%A\n******\n" (automaton9 |> minimize)


