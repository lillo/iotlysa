﻿module AutomataGenerator

open ParseTree
open SymbolTable
open Automata


(*
    TODO: 
      - Scrivere un visitor che per ogni nodo genera le variabili di output quindi Map<Symbol, Set<Symbol list>>
      - Aggiungere alla automata_of_node un nuovo parametro di tipo Map<Symbol, Set<Symbol list>>
      - Generare tante transizioni quante sono le variabili di output
        
 *)

let automata_of_node (comp : Map<Symbol, Symbol list>) (tableVar : Map<Symbol, Set<Symbol list * Symbol list>>) (nodeIndex : Map<Symbol, int>) (node : NodeDecl) = 
    let symName (s : Symbol) = s.ToString().Split(':').[0]
    let symIndex (s : Symbol) = nodeIndex.[s]
    let (NDecl(s, nb)) = node
    let node_id = symName s
    let c = ref(0)
    let nextName () = 
        c := !c + 1
        Printf.sprintf "%d" !c
    let nextAutomata () = 
        Automata.singleton (nextName())

    let rec automata_of_process (proc : ProcessParseTree) = 
        let tupleMsg (delim1 : string) (delim2 : string) (sl : Symbol list) = 
            let slist = List.map symName sl
            Printf.sprintf "%s%s%s" delim1 (String.concat "," slist) delim2
                               
        match proc with
            | PNil -> Automata.empty (nextName())
            | PId(_) -> Automata.singleton (nextName())//nextAutomata ()
            | PAssign(_,_,c) 
            | PCmd(_,_,c) 
            | PTau(c) -> Automata.append (nextAutomata ()) None (automata_of_process c)
            | PCond(_,c1,c2) -> Automata.choice (nextName () |> Automata.makeState, nextName() |> Automata.makeState )
                                                (automata_of_process c1) (automata_of_process c2)
            | PRec(_,c) -> Automata.append (nextAutomata()) None (automata_of_process c) |> Automata.loop
(*            | PReceive(el,sl,c) ->  let sl1 = List.map (fun (App(s,_)) -> s) el
                                    let tm = tupleMsg "(" ")" (List.append sl1 sl)
                                    let ss = List.map (fun s -> Printf.sprintf "%s%s?%s" (symName s) node_id tm) comp.[s]
                                    Automata.append (nextAutomata()) (Some(String.concat "\\n" ss)) (automata_of_process c) 
            | PReceive(el,sl,c) ->  let sl1 = List.map (fun (App(s,_)) -> s) el
                                    let tm = tupleMsg "(" ")" (List.append sl1 sl)
                                    let ss = List.map (fun s -> Some(Printf.sprintf "%s%s?%s" (symName s) node_id tm)) comp.[s]
(* For each neigh in comp.[s] and for each t in tableVar.[neigh] if |t| == |el| + |sl| do tupleMsg "(" ")" t*)
                                    Automata.appendL (nextAutomata()) ss (automata_of_process c)  *)
            | PReceive(el,sl,c) ->  let tupleLength = el.Length + sl.Length
                                    let tuples = [ for neigh in comp.[s] do
                                                   for (r,t) in tableVar.[neigh] do 
                                                   if (List.contains s r) && t.Length = tupleLength then yield tupleMsg "<" ">" t |>  Printf.sprintf "%d ? %s" (symIndex neigh) |> Some]
                                    Automata.appendL (nextAutomata()) tuples (automata_of_process c)
            | PRecv(el,sl,c) ->  let tupleLength = 1 + sl.Length
                                 let tuples = [ for neigh in comp.[s] do
                                                for (r,t) in tableVar.[neigh] do 
                                                if (List.contains s r) && t.Length = tupleLength && t.Head = el then yield tupleMsg "<" ">" t |>  Printf.sprintf "%d ? %s" (symIndex neigh) |> Some]
                                 Automata.appendL (nextAutomata()) tuples (automata_of_process c)
            | PSend(el, sl, c) -> let tm = tupleMsg "<" ">" (List.map (fun (App(s,_)) -> s) el)
                                  let fsms = List.map (fun s -> Automata.append (nextAutomata()) 
                                                                                (Some(Printf.sprintf "%d ! %s" (symIndex  s) tm))
                                                                                (nextAutomata()))
                                                                                 
                                                      sl
                                  //let comp = List.fold Automata.shuffle fsms.Head fsms.Tail
                                  let comp = List.fold (fun a1 a2 -> Automata.append a1 None a2) fsms.Head fsms.Tail
                                  Automata.append comp None (automata_of_process c) 
            | PSnd(lbl, el, sl, c) -> let tm = tupleMsg "<" ">" (lbl :: (List.map (fun (App(s,_)) -> s) el))
                                      let fsms = List.map (fun s -> Automata.append (nextAutomata()) 
                                                                                    (Some(Printf.sprintf "%d ! %s" (symIndex  s) tm))
                                                                                    (nextAutomata()))
                                                          sl
                         //let comp = List.fold Automata.shuffle fsms.Head fsms.Tail
                                      let comp = List.fold (fun a1 a2 -> Automata.append a1 None a2) fsms.Head fsms.Tail
                                      Automata.append comp None (automata_of_process c) 
            | PSwitch(processes) -> List.fold (fun a p -> Automata.choice (nextName () |> Automata.makeState, nextName() |> Automata.makeState ) a (automata_of_process p) ) 
                                              (automata_of_process processes.Head) processes.Tail
                                    

    let mapNodeB (nodeBody : NodeBody) = 
        match nodeBody with 
            | PDecl(p) -> Some(automata_of_process p)
            | _ -> None


    let res = nb |> List.map mapNodeB |> List.filter Option.isSome |> List.map Option.get
    //let comp = List.fold (Automata.shuffle) res.Head res.Tail
    res



    (* 
        let tm = tupleMsg "<" ">" (List.map (fun (App(s,_)) -> s) el)
                                  let ss = List.map (fun s -> Printf.sprintf "%s%s!%s" node_id (symName s) tm) sl
                                  printfn "%A" ss
                                  Automata.append (nextAutomata()) (automata_of_process c) (Some(String.concat "\\n" ss))


        let foldNodeB (a : Automata.automata option) (nodeBody : NodeBody) = 
         match (a, nodeBody) with
            | (Some(a), PDecl(p)) -> Some(Automata.shuffle a (automata_of_process p))
            | (None, PDecl(p)) -> Some(automata_of_process p)
            | _ -> a 
     *)
     
