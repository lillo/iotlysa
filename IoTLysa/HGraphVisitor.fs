﻿module HGraphVisitor

open SymbolTable
open ParseTree

type glabel = Id | AppF of Symbol | Con
              override this.ToString() = 
                match this with
                  | Id -> "id"
                  | Con -> "==="
                  | AppF(s) -> s.ToString()

type FHGraph = Set<Symbol> * Map<Symbol, (glabel * Symbol) list>

let str_of_hgraph ((nodes, arcs) : FHGraph) = 
    let sbuilder = new System.Text.StringBuilder()
    Printf.bprintf sbuilder "Nodes { "

    for n in nodes do
      n.ToString() |> Printf.bprintf sbuilder "%s " 
    
    Printf.bprintf sbuilder "}\nHArcs {\n"

    for (n,l) in Map.toSeq arcs do
      n.ToString () |> Printf.bprintf sbuilder "%s -> { "
      for n' in l do
        n'.ToString() |> Printf.bprintf sbuilder "%s "
      Printf.bprintf sbuilder "}\n"

    Printf.bprintf sbuilder "\n}\n"
    sbuilder.ToString()



let add_farc s e ((nodes, edges) : FHGraph) = 
    let rest = match Map.tryFind s edges with
                   | Some(l) -> l 
                   | _ -> []
    let nnodes = List.fold (fun s (_,sv) -> Set.add sv s) nodes e 
    (Set.add s nnodes, Map.add s (List.append rest e) edges) // Aggiungere gli altri simboli

let add_expression t s e hgraph = 
    //Printf.eprintf "%A\n" e
    let es = match e with
                | App(sv,[]) -> [(t,sv)]
                | App(sf,ls) -> List.map (fun (App(sv,[])) -> (AppF(sf), sv)) ls
    add_farc s es hgraph

let hgraph_of_node (node : NodeDecl) cmap :  FHGraph =
    let (NDecl(_, nb)) = node

    let rec graph_of_process (proc : ProcessParseTree) (graph : FHGraph) = 
        match proc with
            | PNil
            | PId(_) -> graph
            | PCmd(_,_,c) 
            | PTau(c)
            | PRec(_,c)
            | PSnd(_,_,_,c)
            | PRecv(_,_,c)
            | PSend(_, _, c)
            | PReceive(_,_,c) -> graph_of_process c graph
            | PCond(_,c1,c2) -> graph |> graph_of_process c1 |> graph_of_process c2
            | PAssign(s,e,c) -> graph |> add_expression Id s e |> graph_of_process c 
            | PSwitch(processes) -> List.fold (fun s p -> graph_of_process p s) graph processes

    let graph_of_body (graph: FHGraph) (nb : NodeBody) = 
         match nb with
             | PDecl(p) ->  graph_of_process p graph
             | _ -> graph
    let emptyg = (Set.empty,Map.empty)
    let initialg = Map.fold (fun g s e -> add_expression Con s e g) emptyg cmap
    List.fold graph_of_body initialg nb