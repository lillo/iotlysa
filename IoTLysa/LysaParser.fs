﻿module LysaParser

open FParsec
open SymbolTable
open ParseTree

type LysaParser(table : SymbolTable) = class

   // let mutable ss = ""

    let str s = pstring s .>> spaces

    let identifier : Parser<string,unit> = 
        let isIdentifierFirstChar c = isLetter c || c = '_'
        let isIdentifierChar c = isLetter c || c = '_' || isDigit c

        many1Satisfy2L isIdentifierFirstChar isIdentifierChar "identifier" .>> spaces

    let listBetweenStrings sOpen sClose pElement f =
        between (str sOpen) (str sClose)
            (spaces >>. sepBy (pElement .>> spaces) (str "," >>. spaces) |>> f)


    let typeDecl  = 
        let addType id = 
            //((run getPosition ss))
            table.AddSymbol id TypeId
            TypeDecl table.[id]
        str "type" >>. identifier  |>> addType

    let funcDecl = 
        let addFunc name parameters rtype = 
           table.AddSymbols [(name, FuncId) ; (rtype, TypeId)]
           let ps = match parameters with 
                        | Some(pars) -> pars |> List.iter (fun p -> table.AddSymbol p TypeId); pars
                        | None -> []
           FuncDecl(table.[name], ps |> List.map (fun p -> table.[p]), table.[rtype])
        str "func" >>. pipe3 identifier (opt (listBetweenStrings "(" ")" identifier (fun i -> i))) (str ":" >>. identifier) addFunc

    let actDecl = 
        let addAct (name, actions) = 
          table.AddSymbol name ActId
          actions |> List.iter (fun p -> table.AddSymbol p ActionId)
          ActuatorDecl(table.[name], List.map (fun n -> table.[n]) actions)
        str "actuator_type" >>. identifier .>>. listBetweenStrings "{" "}" identifier (fun i -> i) |>> addAct

    let sigDecl = 
        many (choice [typeDecl ; funcDecl  ; actDecl ])

    let sensorDecl = 
        let ssensor, ssensorref = createParserForwardedToRef<SensorParseTree, unit>()
        
        let snil = str "nil" |>> (fun _ -> SNil)
        let sprobe = str "probe" .>> str ";" >>. ssensor |>> SProbe
        let stau = str "tau" .>> str ";" >>. ssensor |>> STau
        let sid = identifier |>> (fun id -> SId(table.AddGetSymbol id RecId))
        let srec = pipe2 (str "rec" >>. identifier .>> str ".") ssensor (fun id s -> SRec(table.AddGetSymbol id RecId, s))
            
        ssensorref := choice [srec; stau ; sprobe ; snil ; sid ]
        pipe3 (str "sensor" >>. identifier ) (str ":" >>. identifier .>> str "=") ssensor 
                (fun sid stype body -> SDecl(table.AddGetSymbol sid SenId, table.AddGetSymbol stype TypeId, body))

    let actuatorDecl =
        let bactuator, bactuatorref = createParserForwardedToRef<ActuatorParseTree, unit>()
        let anil = str "nil" |>> (fun _ -> ANil)
        let atau = str "tau" .>> str ";" >>. bactuator |>> ATau
        let aid = identifier |>> (fun id -> AId(table.AddGetSymbol id RecId))
        let arec = pipe2 (str "rec" >>. identifier .>> str ".") bactuator (fun id s -> ARec(table.AddGetSymbol id RecId, s))
        let await = str "wait_for" >>. listBetweenStrings "(" ")" identifier (fun i -> i) .>> str "; ".>>. bactuator |>> 
                    (fun (l, ba) -> AWaitFor(List.map (fun s -> table.AddGetSymbol s ActionId) l, ba))

        bactuatorref := choice [arec; await; atau; anil; aid]
        pipe3 (str "actuator" >>. identifier ) (str ":" >>. identifier .>> str "=") bactuator 
                (fun sid stype body -> ADecl(table.AddGetSymbol sid ActId, table.AddGetSymbol stype TypeId, body))    

    let expression = 
        let opp = new OperatorPrecedenceParser<Expression,unit,unit>()
        let expr = opp.ExpressionParser
        let lit =  
            let lfloat = (pfloat .>> spaces) |>> fun f -> App (table.AddGetSymbol (f.ToString()) NumericId, [])
            let lbool = (str "false" ) <|> (str "true") |>> fun f -> App(table.AddGetSymbol f FuncId, [])
            lfloat <|> lbool 
    
        let app = pipe2 identifier (opt (listBetweenStrings "(" ")" expr (fun i -> i))) (fun id le-> App(table.AddGetSymbol id FuncId, match le with Some(l) -> l | None -> []))
        let term = lit <|> app <|> between (str "(") (str ")") expr
        opp.TermParser <- term


        opp.AddOperator(InfixOperator("&&", spaces, 1, Associativity.Left, fun x y -> App(table.AddGetSymbol "&&" FuncId, [x;y])))
        opp.AddOperator(InfixOperator("||", spaces, 1, Associativity.Left, fun x y -> App(table.AddGetSymbol "||" FuncId, [x;y])))

        opp.AddOperator(InfixOperator("<", spaces, 2, Associativity.Left, fun x y -> App(table.AddGetSymbol "<" FuncId, [x;y])))
        opp.AddOperator(InfixOperator("=", spaces, 2, Associativity.Left, fun x y -> App(table.AddGetSymbol "=" FuncId, [x;y])))

        opp.AddOperator(InfixOperator("+", spaces, 3, Associativity.Left, fun x y -> App(table.AddGetSymbol "+" FuncId, [x;y])))
        opp.AddOperator(InfixOperator("-", spaces, 3, Associativity.Left, fun x y -> App(table.AddGetSymbol "-" FuncId, [x;y])))

        opp.AddOperator(InfixOperator("*", spaces, 4, Associativity.Left, fun x y -> App(table.AddGetSymbol "*" FuncId, [x;y])))
        opp.AddOperator(InfixOperator("/", spaces, 4, Associativity.Left, fun x y -> App(table.AddGetSymbol "/" FuncId, [x;y])))
    
        opp.AddOperator(PrefixOperator("-", spaces, 5, true, fun x -> App(table.AddGetSymbol "--" FuncId, [x])))
        opp.AddOperator(PrefixOperator("not", spaces, 5, true, fun x -> App(table.AddGetSymbol "not" FuncId, [x])))
        expr

    let processDecl = 
        let pprocess, processref = createParserForwardedToRef<ProcessParseTree, unit>()   

        let pnil = str "nil" |>> fun _ -> PNil

        let ptau = str "tau" >>. str ";" >>.  pprocess |>> PTau

        let pcmd = pipe3 (str "@" >>. identifier .>> str ".") (identifier .>> str ";") pprocess (fun aname aaction cont -> PCmd(table.AddGetSymbol aname ActId, table.AddGetSymbol aaction ActionId, cont))

        let prec = pipe2 (str "rec" >>. identifier .>> str ".") pprocess (fun varname cont -> PRec(table.AddGetSymbol varname RecId, cont))

        let psend = pipe3 (str "send" >>. listBetweenStrings "(" ")" expression (fun i -> i))
                          (str "to" >>. listBetweenStrings "[" "]" identifier (fun i -> i))
                          (str ";" >>. pprocess)
                          (fun args nodes cont -> PSend(args, List.map (fun s -> table.AddGetSymbol s NodeId) nodes, cont))

        let psnd = pipe4 (str "snd" .>> str "(" >>. identifier .>> str ",")
                          //(listBetweenStrings "(" ")" expression (fun i -> i))
                          (sepBy1 expression (str ",") .>> str ")")
                          (str "to" >>. listBetweenStrings "[" "]" identifier (fun i -> i))
                          (str ";" >>. pprocess)
                          (fun lbl args nodes cont -> PSnd(table.AddGetSymbol lbl LabelId, args, List.map (fun s -> table.AddGetSymbol s NodeId) nodes, cont))

        let pcond = pipe3 (str "if" >>. expression) (str "then" >>. pprocess) (str "else" >>. pprocess) (fun e p1 p2 -> PCond(e, p1,p2)) 
        
        let idfact = pipe2 identifier (opt ((str ":=") >>. expression .>> str ";" .>>. pprocess)) (fun i o -> 
                                                                                                     match o with 
                                                                                                        | Some((e,p)) -> PAssign(table.AddGetSymbol i VarId, e, p)
                                                                                                        | _ -> PId({value=i;symbolType=RecId}))

        let preceive = 
            pipe3 (str "receive" >>. str "(" >>. (sepBy expression (str ",")) .>> str ";") (sepBy1 identifier (str ",") .>> str ")" .>> str ";") pprocess (
                             fun pl vl cont -> PReceive(pl, List.map (fun s -> table.AddGetSymbol s VarId) vl, cont))

        let precv = 
            pipe3 (str "recv" >>. str "(" >>. identifier .>> str ";") (sepBy1 identifier (str ",") .>> str ")" .>> str ";") pprocess (
                             fun lbl vl cont -> PRecv(table.AddGetSymbol lbl LabelId, List.map (fun s -> table.AddGetSymbol s VarId) vl, cont))

        let pswitch = str "switch" >>. str "{" >>. (sepBy1 precv (str "|")) .>> str "}" |>> PSwitch
                            
    
        processref := choice [pnil ; ptau; preceive; precv; pswitch; psend; psnd; pcond;  pcmd ; prec; idfact]

        (str "process" .>> str "=") >>. pprocess |>> PDecl

    let nodeDecl = 
        pipe2 (str "node" >>. identifier .>> str "=") (many (choice [processDecl ; sensorDecl; actuatorDecl])) (fun nname body -> NDecl(table.AddGetSymbol nname NodeId, body)) |> many

    let programParser = spaces >>. sigDecl .>>. nodeDecl .>> eof

    member __.Parse(file : string) = 
       // ss <- file
        runParserOnFile ( programParser ) () file System.Text.Encoding.UTF8 
        
        
    member __.ParseOnString(text : string) =
        runParserOnString ( programParser ) () "" text
        
    member __.ParseSignaturesOnString(text : string) = 
        runParserOnString (spaces >>. sigDecl .>> eof) () "" text 

end
