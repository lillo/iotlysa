﻿module NormMsgVisitor

open SymbolTable
open ParseTree


type SymbolGenerator(b : string, symTable : SymbolTable) = 
    class
        let index = ref(-1)
        member this.nextSymbol() = index := !index + 1; symTable.AddGetSymbol (b + (!index).ToString()) VarId 
    end


let normalizeMessage (genSym : SymbolGenerator) (node : NodeDecl)= 
    let mjoin m1 m2 = Map.fold (fun m k v -> Map.add k v m) m1 m2

    let rec normProcess (proc : ProcessParseTree) = 
        match proc with
            | PNil -> (PNil, Map.empty)
            | PTau(c) -> let (p1, m1) = normProcess c 
                         (PTau(p1), m1)
            | PId(_) -> (proc, Map.empty)
            | PAssign(s, e, c) -> let (p1, m1) = normProcess c
                                  (PAssign(s,e, p1), m1)
            | PCond(e,pt,pe) -> let (pt1, m1) = normProcess pt
                                let (pe1, m2) = normProcess pe
                                (PCond(e,pt1,pe1), mjoin m1 m2)    
            | PSend(el, sl, c) -> let (p1, m1) = normProcess(c)
                                  let lm = List.map (fun e -> (genSym.nextSymbol(),e)) el
                                  let vars = List.map (fun (s,_) -> App(s,[])) lm
                                  (PSend(vars, sl, p1), mjoin (Map.ofList lm) m1)
            | PSnd(lbl, el, sl, c) -> let (p1, m1) = normProcess c
                                      let lm = List.map (fun e -> (genSym.nextSymbol(), e)) el 
                                      let vars = List.map (fun (s,_) -> App(s,[])) lm
                                      (PSnd(lbl, vars, sl, p1), mjoin (Map.ofList lm) m1)
            | PRecv(lbl, sl,c) -> let (p1, m1) = normProcess c
                                  let lm = List.map (fun s -> (genSym.nextSymbol(),App(s,[]))) sl
                                  let vars = List.map fst lm
                                  (PRecv(lbl, vars,p1), mjoin (Map.ofList lm) m1)
            | PReceive(el,sl,c) -> let (p1, m1) = normProcess c
                                   let lm = List.map (fun s -> (genSym.nextSymbol(),App(s,[]))) sl
                                   let vars = List.map fst lm
                                   (PReceive(el,vars,p1), mjoin (Map.ofList lm) m1)
            | PCmd(s1,s2,c) -> let (p1, m1) = normProcess c
                               (PCmd(s1,s2,p1),m1)
            | PRec(s, c) -> let (p1,m1) = normProcess c
                            (PRec(s,p1),m1)
            | PSwitch(processes) -> let (receivers,maps) = List.map normProcess processes |> List.unzip
                                    (PSwitch(receivers), List.fold mjoin maps.Head maps.Tail)

    let mapNodeB (nodeBody : NodeBody) = 
        match nodeBody with
            | PDecl(p) -> let (p1, m1) = normProcess p
                          (PDecl(p1), m1)
            | _ -> (nodeBody, Map.empty)
    
    match node with
        | NDecl(s, nb) -> let lp = List.map mapNodeB nb
                          let (lb,lm) = List.unzip lp
                          Printf.printfn "%A" lb
                          (NDecl(s, lb), List.fold mjoin Map.empty lm)


let normalizeVisitor (nodes : NodeDecl list) (symTable : SymbolTable) = 
    let genSym = new SymbolGenerator("m", symTable)
    List.map (normalizeMessage genSym) nodes