﻿module OutputVarsVisitor

open ParseTree
open SymbolTable

let getVars varsTable (NDecl(s, nb)) = 
    let rec normProcess (proc : ProcessParseTree) = 
        match proc with
            | PNil 
            | PId(_) -> Set.empty
            | PTau(c) 
            | PAssign(_, _, c) 
            | PReceive(_,_,c) 
            | PRecv(_,_,c)
            | PCmd(_,_,c)
            | PRec(_, c)        -> normProcess c
            | PCond(e,pt,pe) -> Set.union (normProcess pt) (normProcess pe)
            | PSend(el, sl, c) -> let vars1 = normProcess(c)
                                  let vars = List.map (fun (App(s,_)) -> s) el //|> Set.ofList
                                  Set.add (sl,vars) vars1
            | PSnd(lbl, el, sl, c) -> let vars1 = normProcess(c)
                                      let vars = lbl :: List.map (fun (App(s,_)) -> s) el //|> Set.ofList
                                      Set.add (sl,vars) vars1
            | PSwitch(processes) -> List.map normProcess processes |> Set.unionMany
            
    let mapNodeB (nodeBody : NodeBody) = 
        match nodeBody with
            | PDecl(p) -> normProcess p
            | _ -> Set.empty
    
    let vars = List.map mapNodeB nb  |> Set.unionMany
    Map.add s vars varsTable



let outputVarsVisitor (nodes : NodeDecl list) = 
    List.fold getVars Map.empty nodes
