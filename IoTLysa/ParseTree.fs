﻿module ParseTree

open SymbolTable

type SigDelaration = TypeDecl of SymbolTable.Symbol 
                   | FuncDecl of Symbol * Symbol list * Symbol
                   | ActuatorDecl of Symbol * Symbol list

type SensorParseTree =    STau of SensorParseTree
                        | SProbe of SensorParseTree
                        | SRec of Symbol * SensorParseTree 
                        | SId of Symbol
                        | SNil

type ActuatorParseTree =  ATau of ActuatorParseTree
                        | AId of Symbol 
                        | AWaitFor of Symbol list * ActuatorParseTree
                        | ARec of Symbol * ActuatorParseTree
                        | ANil

type Expression = App of Symbol * (Expression list)

type ProcessParseTree =   PTau of ProcessParseTree
                        | PId of Symbol 
                        | PAssign of Symbol * Expression * ProcessParseTree
                        | PCond of Expression * ProcessParseTree * ProcessParseTree
                        | PSend of Expression list * Symbol list * ProcessParseTree
                        | PSnd of Symbol * Expression list * Symbol list * ProcessParseTree
                        | PReceive of Expression list * Symbol list * ProcessParseTree
                        | PRecv of Symbol * Symbol list * ProcessParseTree
                        | PCmd of Symbol * Symbol * ProcessParseTree
                        | PRec of Symbol * ProcessParseTree
                        | PSwitch of ProcessParseTree list
                        | PNil


type NodeBody = SDecl of Symbol * Symbol * SensorParseTree 
              | ADecl of Symbol * Symbol * ActuatorParseTree
              | PDecl of ProcessParseTree

type NodeDecl = NDecl of Symbol * NodeBody list
