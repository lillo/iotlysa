﻿open SymbolTable
open LysaParser
open ParseTree
open TypeChecker
open TypeChecker.Algorithms.Program
open FParsec
open NormMsgVisitor
open Automata
open AutomataGenerator
open OutputVarsVisitor
open TypeChecker.Algorithms


let elaborateNode compTable varTable nodeIndex ((NDecl(n,_) as node), cmap) =  
    let symName = n.ToString().Split(':').[0]
    let frontierVariables = cmap |> Map.toSeq |> Seq.map fst |> Set.ofSeq
    let hyperGraph = HGraphVisitor.hgraph_of_node node cmap

    let c = ref(-1)
    let nextName () = 
        c := !c + 1
        Printf.sprintf "q%d" (!c)
    
    let automata = AutomataGenerator.automata_of_node compTable varTable nodeIndex node |> List.map (fun a -> (a, Automata.removeTau a))
    for (a1,a2) in automata do
        Printf.printfn "%s\n\n" <| Automata.toGraphViz (a2 |> Automata.rename nextName)
        Printf.printfn "%s\n\n" <| Automata.toGMC symName (a2 |> Automata.rename nextName)
    

    //let automata = AutomataGenerator.automata_of_node compTable varTable nodeIndex node |> List.map Automata.removeTau
    let shuffing = List.fold (fun a1 a2 -> Automata.merge a1 (snd a2)) (snd automata.Head) automata.Tail |> Automata.rename nextName 
    //let shuffing = List.fold (fun a1 a2 -> Automata.shuffle a1 a2) automata.Head automata.Tail |> Automata.rename nextName 
    (frontierVariables, hyperGraph, shuffing, symName)               




let manipulate ((_, nodes : NodeDecl list)) (symTable : SymbolTable) compTable =  //(compTable : Map<Symbol, Symbol list>)
(*    let compTable = [ (symTable.["N_cp"], []) ;
                      (symTable.["N_la"], [symTable.["N_cp"]]) ;
                      (symTable.["N_pd"], [symTable.["N_la"]]) ;
                      (symTable.["N_ls"], [symTable.["N_la"] ; symTable.["N_Lamp1"] ; symTable.["N_Lamp2"] ; symTable.["N_Lamp3"]]) ;
                      (symTable.["N_Lamp1"], [symTable.["N_ls"]]) ;
                      (symTable.["N_Lamp2"], [symTable.["N_ls"] ; symTable.["N_Lamp1"]]) ;
                      (symTable.["N_Lamp3"], [symTable.["N_ls"] ; symTable.["N_Lamp1"] ; symTable.["N_Lamp2"]])
                     ] |> Map.ofList
*)    
    let nnodes = NormMsgVisitor.normalizeVisitor nodes symTable 
    let nodeIndex = List.unzip nnodes |> fst |> List.map (fun (NDecl(n,_)) -> n) |> (fun l -> List.zip l ([ 0 .. l.Length - 1])) |> Map.ofList
    let tableVars = OutputVarsVisitor.outputVarsVisitor (List.unzip nnodes |> fst)
    nnodes |> List.map (elaborateNode compTable tableVars nodeIndex)


let showResult s results : unit = 
    let debugBuilder = new System.Text.StringBuilder()
    let GMCBuilder = new System.Text.StringBuilder()
    let iter (frontierVariables, hyperGraph, shuffing, name) = 
        Printf.bprintf debugBuilder "##### Automaton %s #####\nFrontier variables\n\n%A\n\nHypergraph\n%s\n\nGraphiz\n%s\n\n" name frontierVariables (HGraphVisitor.str_of_hgraph hyperGraph) (Automata.toGraphViz shuffing)
        Printf.bprintf GMCBuilder "%s\n\n" (Automata.toGMC name shuffing)
    use debugStream = new System.IO.StreamWriter(s.ToString().Split('.').[0] + ".debug")
    use GMCStream = new System.IO.StreamWriter(s.ToString().Split('.').[0] + ".fsa")
    results |> List.iter iter
    debugStream.Write(debugBuilder)
    GMCStream.Write(GMCBuilder)

let readComp file (symTable : SymbolTable) = 
     
    let parseLine (line : string) = 
        let split = line.Split('-')
        let list = if split.Length > 1 then [ for si in split.[1].Trim().Split(' ') do if si <> "" then yield symTable.[si] ] else []
        try
        (symTable.[split.[0].Trim()], list)
        with _ -> printfn "Not Found %s\n" (split.[0].Trim()); failwith "Not found"
    System.IO.File.ReadLines file  |>
    Seq.filter ((<>) "") |>
    Seq.map parseLine |>
    Map.ofSeq

type ProgramOptions =   
                    { 
                        lysaFile : string
                        compFile : string
                        doAutoma : bool
                        doTypeCheck : bool
                        showTypeCheckWarning : bool
                        showTypeCheckDebug : bool
                    }

[<EntryPoint>]
let main argv = 
    //testAutomata ()
    //exit 0
    let symTable = SymbolTable()
    let lysaParser = LysaParser(symTable)

    let typeCheck ( (sigs, ndecls) : SigDelaration list * NodeDecl list )
                  ( opt:ProgramOptions ) =
        printfn "Type checking"
        let tcheckRes = Checker symTable sigs ndecls
        if opt.showTypeCheckWarning && tcheckRes.inferenceWarning then
            printfn "The type checking was passed but with some warning."
            printfn "Follow the list of nodes that contains some variables with not infered type: "
            tcheckRes.nodes |>
            Map.filter ( fun _ (v:TypeCheckerNode) -> v.InferenceWarning ) |>
            Map.iter( fun k v -> printfn "\n\t%s --> %A" k.value v.notInfered)
            if opt.showTypeCheckDebug then //The follow print is thought only as internal break of TC printing.
                printfn "\n-----------------------------\n"
        
        if opt.showTypeCheckDebug then
            printfn "%A" tcheckRes 


    let elaborate (opt:ProgramOptions) = 
        printfn "Opening %s" opt.lysaFile
        printfn "Elaborating %s" opt.lysaFile
        let parseResult = lysaParser.Parse opt.lysaFile

        match parseResult with
            | Success(result, _, _) ->
                if opt.doTypeCheck then
                    typeCheck result opt

                if opt.doAutoma then
                    let scomp = readComp opt.compFile symTable  
                    printfn "Compatibility: loaded"
                    manipulate result symTable scomp |> (showResult opt.lysaFile)
            | Failure(error,_,_) -> 
                printfn "Failure %A" error


    if Array.isEmpty argv || argv.Length > 2 then
        Printf.eprintf "Usage:\n <lysa specification> [<compatibility>]\n" 
        //Printf.eprintf "Usage:\n%s <lysa specification> <compatibility>\n" <| System.Environment.GetCommandLineArgs().[0]
        exit 1
    else
        let opt = 
            if argv.Length = 1 then
                { 
                    lysaFile = argv.[0]
                    compFile = ""
                    doAutoma = false
                    doTypeCheck = true
                    showTypeCheckWarning = true
                    showTypeCheckDebug = true
                }
            else
                {
                    lysaFile = argv.[0]
                    compFile = argv.[1]
                    doAutoma = true
                    doTypeCheck = true
                    showTypeCheckWarning = true
                    showTypeCheckDebug = true
                }
        try
            elaborate opt
        with e -> 
            printfn "%s" e.Message
            exit 2
        exit 0


