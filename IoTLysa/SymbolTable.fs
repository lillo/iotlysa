﻿module SymbolTable

open System.Collections.Generic

(* The type used for the lexeme part of the symbols. *)
type Lexeme = string

type SymbolType = TypeId | VarId | FuncId | ActId | ActionId | RecId | SenId | NodeId | LabelId | NumericId

(*
Here follows the new implementation.
The new implementation differs from the previous only in the compare behaviour since the old choice
was compare only on value attribute, instead now we're using a natural compare behaviour
of records that compare on all attributes.

Basically, before two symbols with the same value but different types was judged equals
but different with the new implementation.

Fortunately this is not a problem since within the SymbolTable the dictionary
is actually only private access and the method responsabile for adding a symbol
first of all check if a key is already used and the key is the string value, a primitive type.
So the behaviour of the SymbolTable is the same and we have also have a more natural
comparing of symbols.
*)
type Symbol = { value:Lexeme; symbolType:SymbolType }
    with
        override this.ToString () = this.value
        member this.ToLongString () = this.value + ":" + this.symbolType.ToString() 

let makeSymbol(v, t) = { value = v; symbolType = t}

type SymbolTable() = class
    let table = new Dictionary<string, Symbol>()

    member __.AddSymbol (svalue: string) (stype : SymbolType) = 
        if not (table.ContainsKey(svalue)) then table.Add(svalue, {value = svalue; symbolType=stype}) 

    member this.AddGetSymbol (svalue: string) (stype : SymbolType) =  
        this.AddSymbol svalue stype 
        this.[svalue]

    member this.AddSymbols (symbols : (string * SymbolType) list ) =
        symbols |> List.iter (fun (s,t) -> this.AddSymbol s t)

    member __.Item
        with get(index) = table.[index]

    member __.Contains = table.ContainsKey

    member __.GetEnumerator = table.GetEnumerator

    member __.Symbols : seq<Symbol> = 
        Seq.map (fun (pair:KeyValuePair<string,Symbol>) -> pair.Value) table

end