namespace TypeChecker

[<AutoOpenAttribute>]
module Domain =

    type Ide = string
    type Type = Type of string
    type TypeVar = TypeVar of int
    type Func = Func of string * Type list * Type
    type Action = Action of string
    type Actuator = Actuator of string * Set<Action>

    type TEnvIm =
        | TEImType of Type
        | TEImFunc of Func
        | TEImActuator of Actuator
        | TEImAction of Actuator

    type TEnv = Map<Ide, TEnvIm>

    type TStoreIm = 
        | NODEID
        | REC
        | Sensor of Type
        | TSImType of Type
        | TSImTypeVar of TypeVar
        | TSImActuator of Actuator

    type TStore = Map<Ide, TStoreIm>

    type TypeCheckerNode =   { 
                                tstore : TStore
                                notInfered : Ide list
                             }
        with 
            member this.InferenceWarning = not <| List.isEmpty this.notInfered


    type TypeCheckerResult = { 
        tenv : TEnv
        nodes : Map<SymbolTable.Symbol, TypeCheckerNode>
        inferenceWarning : bool
    }
