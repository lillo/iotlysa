namespace TypeChecker

open SymbolTable

[<AutoOpenAttribute>]
module Exceptions =

    (*
        For each exception is given the symbol that rise the exception
        and in which SigDelaration is symbol was found.

        This structure is very useful especially for the DoubleActionException.
    *)

    /// A signature has a duplicated name.
    exception NameDuplicationException of sym:Symbol
        with 
            override this.Message = 
                sprintf "ERROR: TYPE_CHECK_1: the name \"%s\" is already used." this.sym.value 

    /// A signature try to use a type not previously declared.
    exception TypeNotDeclaredException of sym:Symbol
        with 
            override this.Message = 
                sprintf "ERROR: TYPE_CHECK_2: the type \"%s\" is not declared." this.sym.value 

    /// A signature use a name reserved as keyword.
    exception KeywordDuplicationException of sym:Symbol
        with 
            override this.Message = 
                sprintf "ERROR: TYPE_CHECK_3: the name \"%s\"  is already a keyword." this.sym.value 

    /// A actuator's signature has duplicate actions.
    exception DoubleActionException of sym:Symbol 
        with 
            override this.Message = 
                sprintf "ERROR: TYPE_CHECK_4: the action \"%s\" is twice." this.sym.value

    /// An expression is not evaluated correctly in a Type value.
    exception VariableEvaluatingException of sym:Symbol
        with
            override this.Message =
                sprintf "ERROR: TYPE_CHECK_5: the variable %s does not evaluates into a Type value." this.sym.value

    /// A name is not declared neither within the type tenvironment nor the tstore
    exception NotDeclaredNameException of sym:Symbol
        with
            override this.Message =
                sprintf "ERROR: TYPE_CHECK_6: usage of %s without declaration." this.sym.value

    /// A name seems indicates a desired usage of a constant but there isn't that constant.
    exception NotDeclaredConstantException of sym:Symbol
        with
            override this.Message =
                sprintf "ERROR: TYPE_CHECK_7: the name %s is not a declared constant." this.sym.value

    /// For indicate a invalid usage of a function, i.e. arguments doesn't match.
    exception ArgumentsFunctionMatchException of sym:Symbol
        with
            override this.Message =
                sprintf "ERROR: TYPE_CHECK_8: invalid call for a function %s." this.sym.value

    /// For indicate a invalid usage of a function, i.e. arguments doesn't match.
    exception NotDeclaredFunctionException of sym:Symbol
        with
            override this.Message =
                sprintf "ERROR: TYPE_CHECK_9: %s is not a function." this.sym.value

    /// For indicate a invalid usage of an action.
    exception UnexpectedActionException of sym:Symbol * actuator:Actuator
        with
            override this.Message =
                let nameActuator = match this.actuator with Actuator(name, _) -> name
                sprintf "ERROR: TYPE_CHECK_10: %s is not an action for %s." this.sym.value nameActuator

    /// For indicate a invalid usage of an assignment.
    exception UnexpectedAssignmentException of sym:Symbol
        with
            override this.Message =
                sprintf "ERROR: TYPE_CHECK_11: unexpected assignment for %s." this.sym.value

    /// For indicate a invalid recipient process id.
    exception UnexpectedRecipientException of sym:Symbol
        with
            override this.Message =
                sprintf "ERROR: TYPE_CHECK_12: %s is an unexpected recipient." this.sym.value

    /// For indicate that a recipient variable within a receive is twice.
    exception DuplicateRecipientException
        with
            override __.Message =
                sprintf "ERROR: TYPE_CHECK_13: there is a receive with a duplicate recipient"

    /// For indicates that a recursive variable is not used anywhere.
    exception NotUsedRecursiveException of sym:Symbol
        with
            override this.Message =
                sprintf "ERROR: TYPE_CHECK_14: the recursive variable %s is declared but not used." this.sym.value

    /// For indicates that guard of a condition is not boolean typed.
    exception NotBooleanConditionException of expr:ParseTree.Expression
        with
            override __.Message =
                sprintf "ERROR: TYPE_CHECK_15: the guard of the condition statement is not boolean typed."