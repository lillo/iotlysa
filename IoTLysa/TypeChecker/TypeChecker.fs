namespace TypeChecker.Algorithms

open TypeChecker.Utils
open TypeChecker
module Program =

    open SymbolTable
    open ParseTree

    let internal checkerBody (tenv:TEnv) (tstore:TStore) body =
        match body with
            | SDecl(name, t, sdecl) ->
                if IsKeyword name.value then
                    raise <| KeywordDuplicationException name

                if tenv.ContainsKey name.value || tstore.ContainsKey name.value then
                    raise <| NameDuplicationException name
                
                match tenv.TryFind t.value with
                | Some( TEImType t ) ->
                    let tstore = tstore.Add(name.value, Sensor t)
                    Sensor.Checker tenv tstore sdecl |> ignore
                    tstore
                | Some _ | None -> raise <| TypeNotDeclaredException t

            | ADecl(name, actuator, adecl) ->
                if IsKeyword name.value then
                    raise <| KeywordDuplicationException name
                   
                if tenv.ContainsKey name.value || tstore.ContainsKey name.value then
                    raise <| NameDuplicationException name

                match tenv.TryFind actuator.value with
                | Some( TEImActuator a ) ->
                    let tstore = tstore.Add( name.value, TSImActuator a)
                    Actuator.Checker tenv tstore (adecl,a) |> ignore
                    tstore
                | Some _ -> raise <| NameDuplicationException actuator
                | None -> raise <| NotDeclaredNameException actuator
            
            | PDecl pdecl -> Process.Checker tenv tstore pdecl


    let internal notInferedIdes (tstore:TStore) = 
        tstore |> Map.filter
                 ( fun _ t -> match t with 
                                | TSImTypeVar _ -> true
                                | _ -> false ) |>
                 Map.toList |>
                 List.map (fun (k, _) -> k)
                 

    let internal sortBodies b1 b2 =
        match b1, b2 with
            | PDecl _, PDecl _ -> -1 //this is to remain the same order for PDecl instances
            | PDecl _, _ -> 1
            | _ -> -1

    let Checker (symTable :  SymbolTable)
                (sigs : SigDelaration list)
                (ndecls : NodeDecl list)  : ( TypeCheckerResult ) =

        let tenv = SigDelaration.Checker (makeTEnvStart(symTable)) sigs
        let gstore = GlobalState tenv ndecls
        let mutable nodes = Map.empty
        let mutable inferenceWarning = false

        for ( NDecl(nodeId, bodies) ) in ndecls do
            //See the rule system. The order matter, the processes for last!
            let nodeStore = bodies |>
                            List.sortWith sortBodies |>
                            List.fold (checkerBody tenv) gstore

            
            let tchecknode = {
                tstore = nodeStore
                notInfered = notInferedIdes nodeStore
            }

            nodes <- nodes.Add(  nodeId, tchecknode )
            inferenceWarning <- inferenceWarning || tchecknode.InferenceWarning

        { tenv = tenv; nodes = nodes; inferenceWarning = inferenceWarning }