namespace TypeChecker

open ParseTree

[<AutoOpenAttribute>]
module Utils =

    open SymbolTable

    let NumericName = "number"
    let NumericType = Type( NumericName ) 
    
    let BoolName = "bool"
    let BoolType = Type( BoolName )

    let FalseName = "false"
    let TrueName = "true"

    let VoidName = "void"
    let VoidType = Type( VoidName )

    let EqualName = "="

    /// It makes a starting env in base of the symbol table that includes numeric constants.
    let makeTEnvStart (table:SymbolTable) : TEnv =
        let tEnvStart : TEnv =
            Map <| seq [
                    BoolName,       TEImType BoolType
                    NumericName,    TEImType NumericType
                    VoidName,       TEImType <| VoidType

                    FalseName,    TEImFunc <| Func( FalseName, [], BoolType )
                    TrueName,     TEImFunc <| Func( TrueName, [], BoolType )
                    
                    "&&",       TEImFunc <| Func( "&&", [ BoolType; BoolType ], BoolType)
                    "||",       TEImFunc <| Func( "||", [ BoolType; BoolType ], BoolType)
                    
                    "+",        TEImFunc <| Func( "+", [ NumericType; NumericType ], NumericType)
                    "-",        TEImFunc <| Func( "-", [ NumericType; NumericType ], NumericType)
                    "*",        TEImFunc <| Func( "*", [ NumericType; NumericType ], NumericType)
                    "/",        TEImFunc <| Func( "/", [ NumericType; NumericType ], NumericType)
                    
                    "<",        TEImFunc <| Func( "<", [ NumericType; NumericType ], BoolType)
                    //EqualName,        TEImFunc <| Func( EqualName, [ NumericType; NumericType ], BoolType)

                    "--",       TEImFunc <| Func( "--", [ NumericType], NumericType)
                    "not",      TEImFunc <| Func( "not", [ BoolType ], BoolType)
                ]
        table.Symbols |>
        Seq.filter ( fun sym -> sym.symbolType = NumericId ) |>
        Seq.map (fun sym -> 
                    (sym.value, TEImFunc( Func(sym.value, [], NumericType) ) ) 
                ) |>
        Seq.fold (fun (s:TEnv) p -> s.Add p ) tEnvStart

    /// Function that checks if a identifier name is keyword. 
    let IsKeyword : (Ide -> bool) =
        (* 
            Here are listed all the possibile strings that are
            recognizable but are actually considered keyword of IoTLysa.

            Pay attention to the fact that here are considered keyword
            also the primitive types. This is because it happens that
            when we require that an object identifier(e.g an action) is not a keyword actually
            we mean neither a primitive type too.
         *)
        let keyWords = 
            [| 
                BoolName; NumericName; VoidName
                "type"; "func"; "actuator_type"; "rec"
                "sensor"; "actuator"; "process"; "node"
                "receive"; "send"; "snd"; "rcv"; "wait_for"
                "probe"; "to"; "if"; "then"; "else"; "switch"
                "nil"; "tau"; TrueName; FalseName; "not"
            |] 

        let h = new System.Collections.Generic.HashSet<string>( keyWords )
        h.Contains

    /// Function that retrieves a possible Type value from an environment
    (* If the indentifier indicated is bounded to a Type(t) value, the value,
       An optional wrapper for that value is given.
       None otherwise.

       It raises no exception at all.
    *)
    let GetType (tenv : TEnv) (s : Ide) =
        match tenv.TryFind s with
            | Some( TEImType t ) -> Some t
            | _ -> None


    /// The function gets a gloabl tstore with the bindings of node-ids declared.
    let GlobalState (tenv : TEnv) (ndecls : NodeDecl list) : TStore = 
        List.fold 
            ( fun (store:TStore) ((NDecl(nodeId, _))) ->  
                // test if the node-id is a keyword.
                if IsKeyword nodeId.value then
                    raise <| KeywordDuplicationException nodeId
               
                // test if there is already declared a duplicate node-id
                if store.ContainsKey nodeId.value then
                    raise <| NameDuplicationException nodeId
                
                // test if the node-id is already used for a object about the env.
                if tenv.ContainsKey nodeId.value then
                    raise <| NameDuplicationException nodeId

                store.Add (nodeId.value, NODEID)
            )
            (Map.empty : TStore)
            ndecls
    

    /// It furnishes a new fresh TypeVar. Uniqueness guaranteed if only this function is used.
    let freshTypeVarGenerator = 
        let mutable freshId = -1
        fun () ->
            freshId <- freshId + 1
            TypeVar(freshId)


    //The function substituites within the store all the same generics value with a new value
    let substituiteAll (tstore:TStore) (oldValue:TypeVar) (newValue:TStoreIm) : TStore = 
        tstore |> 
            Map.filter ( fun _ v -> v = TSImTypeVar(oldValue) ) |>
            Map.fold ( fun s k _ -> s.Add(k, newValue) ) tstore

