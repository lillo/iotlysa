namespace TypeChecker.Algorithms

module Actuator =

    open SymbolTable
    open ParseTree
    open TypeChecker

    (*
       There is an assumption that the store contains its actuator declaration,
       this because the declaration in the parse tree is in the upper level.
    *)
    /// The function type check an actuator declaration with a given env and store.
    let Checker (tenv : TEnv ) (tstore : TStore)
                              (     (tree, (Actuator( _, actions) as actu) )
                                        : ActuatorParseTree * Actuator
                              )
                                : TStore =

        let isInvalidAction (act:Symbol) = not <| actions.Contains (Action act.value)

        // the function recursively check the ActuatorParseTree but for fixed actuator.
        let rec checkerFixedActuator (tenv:TEnv) (tstore:TStore) tree =
            match tree with
            | ANil -> tstore
            | ATau(A) -> checkerFixedActuator tenv tstore A
            | AWaitFor( actList, A ) ->
                match List.tryFind isInvalidAction actList with
                | Some( invalidAct ) -> 
                    if IsKeyword invalidAct.value then
                        raise <| KeywordDuplicationException invalidAct

                    raise <| UnexpectedActionException (invalidAct, actu)
                | None -> checkerFixedActuator tenv tstore A
            | ARec(h, A) ->
                if IsKeyword h.value then
                    raise <| KeywordDuplicationException h

                if (tenv.ContainsKey h.value) || (tstore.ContainsKey h.value) then
                    raise <| NameDuplicationException h

                checkerFixedActuator tenv (tstore.Add (h.value, REC)) A
            | AId(h) ->
                if IsKeyword h.value then
                    raise <| KeywordDuplicationException h

                match tstore.TryFind h.value with
                | Some(REC) -> tstore
                | Some(_) -> raise <| NameDuplicationException h            
                | None -> 
                        if tenv.ContainsKey h.value then
                            raise <| NameDuplicationException h
                        else
                            raise <| NotDeclaredNameException h

        checkerFixedActuator tenv tstore tree