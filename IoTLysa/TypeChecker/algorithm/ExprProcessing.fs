namespace TypeChecker.Algorithms

open SymbolTable
module Expression =

    open ParseTree
    open TypeChecker

    (*
        The code needs an introduction or it seems more cumbersome that is so seem.

        Unfortunately, a constant is modeled within the abstract parser tree as
        a function without arguments. There is no difference at all!!

        So in the following code I have to manage two cases with one when the arguments
        of the App instance is an empty list! Is it a name of variable or a constant?!?
        This is the reason why in the first match we involve both the store and the env contains.

        Since the expression must evaluate in a Type, there are only two cases if the name is
        in the store: there is a variable with a type value or a sensor.

        Otherwise, I just see if the name is in the env as a constant value i.e. a function with no args.
        If there is actually a function bounded with that name but not as a constant I can be sure that
        the user used wrong the name as a function because by construnction of env and store the names
        of the twos are always disjuncted.

        Finally if there is any function declared with that name I check the list arguments is
        evaluated as in App instance.
    *)
    let rec  Checker (tenv : TEnv) (tstore : TStore)
                            (expr : Expression) : (TStoreIm * TStore) =
        match expr with
        | App( name, [] ) when tstore.ContainsKey name.value -> // variable case.
            match tstore.[name.value] with
            | (TSImType _) as t -> (t, tstore)
            | (TSImTypeVar _) as t -> (t, tstore)
            | (Sensor t) -> (TSImType t, tstore)
            | _ ->  if IsKeyword name.value then
                        raise <| KeywordDuplicationException name
                    raise <| VariableEvaluatingException name

        | App( name, [] ) when tenv.ContainsKey name.value -> // constant case.
            match tenv.[name.value] with
            | TEImFunc( Func( _, [], tret) ) -> (TSImType tret, tstore)
            | _ ->  if IsKeyword name.value then
                        raise <| KeywordDuplicationException name
                    raise <| NotDeclaredConstantException name

        //eq operator with two operands
        | App( { Symbol.symbolType = FuncId; Symbol.value = v} as op, [left; right] ) when v = EqualName ->
            let (left, tstore) = Checker tenv tstore left
            let (right, tstore) = Checker tenv tstore right

            //Removing of possible Sensor constructor's wrapper.
            //This will simplify the next match statement.
            //let left = match left with | Sensor v -> TSImType v | _ -> left
            //let right = match right with | Sensor v -> TSImType v | _ -> right

            let tstore =
                match (left, right) with
                | (TSImType t1, TSImType t2) when t1=t2 -> tstore
                | (TSImTypeVar tvar, v) | (v, TSImTypeVar tvar) -> substituiteAll tstore tvar v
                | _ -> raise <| ArgumentsFunctionMatchException op

            (TSImType BoolType, tstore)

        //eq operator not with two operands
        | App( { Symbol.symbolType = FuncId; Symbol.value = v} as op, _) when v = EqualName ->
            raise <| ArgumentsFunctionMatchException op

        | App( name, args ) when tenv.ContainsKey name.value -> // function case && args<>[]
            match tenv.[name.value] with
            | TEImFunc( Func( _, targs, tret ) ) ->
                // the lenght of actual and formal parameters must be the same.
                if args.Length <> targs.Length then
                    raise <| ArgumentsFunctionMatchException name

                let finalStore =
                    List.fold2
                        ( fun (curStore:TStore) (App(nameArg, _) as actualArg ) formalPar ->
                            match curStore.TryFind nameArg.value with
                            | Some( TSImTypeVar genericValue ) ->
                                substituiteAll curStore genericValue formalPar

                            | _ ->
                                let (argEval, storeEval) = Checker tenv curStore actualArg
                                if argEval <> formalPar then
                                    raise <| ArgumentsFunctionMatchException name
                                storeEval
                        )
                        tstore
                        args // the argument expressions
                        (List.map TSImType targs) // the argument type expected mapped in TSImtype's


                (TSImType tret, finalStore)
            | _ -> raise <| NotDeclaredFunctionException name

        | App( name, [] ) ->
            if IsKeyword name.value then
                raise <| KeywordDuplicationException name
            raise <| NotDeclaredNameException name

        | App( name, _ ) ->
            if IsKeyword name.value then
                raise <| KeywordDuplicationException name
            raise <| NotDeclaredFunctionException name

    and

        CheckerListStore (tenv : TEnv) (tstore : TStore)
                            (exprList : Expression list) : (TStore) =
            // The choice of REC value doesn't matter.
            // The first component of the state will never be used.
            List.fold ( fun s e -> snd (Checker tenv s e) ) tstore exprList
