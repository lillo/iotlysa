namespace TypeChecker.Algorithms

module Process =

    open ParseTree
    open TypeChecker

    let rec Checker (tenv : TEnv) (tstore : TStore) (proc : ProcessParseTree) : TStore =
        match proc with
        | PNil -> tstore
        | PTau(P) -> Checker tenv tstore P
        | PRec(h, P) -> 
                    if IsKeyword h.value then
                        raise <| KeywordDuplicationException h

                    if (tenv.ContainsKey h.value) then
                        raise <| NameDuplicationException h

                    match tstore.TryFind h.value with
                    | Some( REC ) -> Checker tenv tstore P
                    | Some _ -> raise <| NameDuplicationException h
                    | None -> Checker tenv (tstore.Add (h.value, REC)) P
        | PId(h) -> 
                    if IsKeyword h.value then
                        raise <| KeywordDuplicationException h

                    match tstore.TryFind h.value with
                    | Some(REC) -> tstore
                    | Some(_) -> raise <| NameDuplicationException h            
                    | None -> 
                        if tenv.ContainsKey h.value then
                            raise <| NameDuplicationException h
                        else
                            raise <| NotDeclaredNameException h
        | PSwitch( pcases ) -> CheckerList tenv tstore pcases

        (* Here the following about PAssign:
            1. we check if the name left is new and if is is we create it.
            2. if it is already in the store it must not be a declared sensor
                because I can't assign a sensor variable.
            3. if it is not a sensor we check the two sides are the same type.
            4. if we need we do a unification/instantiation of variable type value.
        *)
        | PAssign(left, expr, P) when (tstore.ContainsKey left.value) -> 
            let (eval, estore) = Expression.Checker tenv tstore expr

            //The left value must be a type value variable.
            match estore.[left.value] with
            | TSImType _ as t ->
                (*
                    ASSERT: The left value is a declared variable with type!!

                    The following match means that if the right value is a TypeVar v
                    we substituites all instances of the bounded value v with
                    the left type of the assignment.
                *)
                match eval with
                | TSImTypeVar v -> (substituiteAll estore v t)
                | _ -> 
                    //This discard the case when the right value are sensors o anything else.
                    if t <> eval then
                        raise <| UnexpectedAssignmentException left
                    
                    //left there already exists and types match, so nothing to add to store
                    estore 
                
            | TSImTypeVar genericValue ->
                //this overwrintg bound for left makes a possible type unification:
                //UNIFICATION if the expression was another variable type value.
                //INSTANTIATION if the expression returns a type value.
                Checker tenv  
                        (substituiteAll estore genericValue eval)
                        P
            | Sensor _ | _ -> raise <| UnexpectedAssignmentException left
            
        | PAssign(left, expr, P) (*when ( not (store.ContainsKey left.value) )*) ->
            if IsKeyword left.value then
                raise <| KeywordDuplicationException left

            if (tenv.ContainsKey left.value) || (tstore.ContainsKey left.value) then
                raise <| NameDuplicationException left

            let (eval, estore) = Expression.Checker tenv tstore expr
            Checker tenv ( estore.Add(left.value, eval) ) P

        | PSend( exprList, ids, P ) ->
            match ids |> List.tryFind 
                            ( fun nIdSym -> 
                                if IsKeyword nIdSym.value then
                                    raise <| KeywordDuplicationException nIdSym

                                match tstore.TryFind nIdSym.value with
                                | Some(NODEID) -> false
                                | _ -> true
                            ) 
                    with
            | Some notValidId -> 
                raise <| UnexpectedRecipientException notValidId
            | None ->
                let estore = Expression.CheckerListStore tenv tstore exprList
                Checker tenv estore P            

        | PSnd(l, exprList, ids, P) ->
            match tenv.TryFind l.value, IsKeyword l.value with
            | Some( TEImFunc( Func(_, [], _) ) ), _ -> // label declared as a constant
                Checker tenv tstore (PSend(exprList, ids, P))
            | _, true -> raise <| KeywordDuplicationException l
            | _ -> raise <| NotDeclaredConstantException l

        | PReceive(exprList, varList, P) ->
            if varList.Length <> (List.distinct varList).Length then
                raise DuplicateRecipientException 

            let tstore = Expression.CheckerListStore tenv tstore exprList

            let tstore = 
                varList |> 
                List.filter 
                    (fun varSym -> 
                        if IsKeyword varSym.value then
                                raise <| KeywordDuplicationException varSym

                        if tenv.ContainsKey(varSym.value) then
                            raise <| UnexpectedAssignmentException varSym

                        //assignments are allowed only for variables that are Type or TypeVar values.
                        //The filter allows to pass only the name not yet in the store and so
                        //we can create a suitable fresh TypeVar for them.
                        //for any other case, e.g. Sensor variabiable ---> raise exception.
                        match tstore.TryFind varSym.value with
                        | Some(TSImType _) -> false
                        | Some(TSImTypeVar _) -> false
                        | None -> true
                        | _ -> raise <| UnexpectedAssignmentException varSym
                    ) |>
                (   
                    tstore |> 
                    List.fold 
                        (fun s varSym ->
                            s.Add(varSym.value, TSImTypeVar (freshTypeVarGenerator()) )
                        )
                )

            Checker tenv tstore P
            
        | PRecv(l, varList, P) ->
            match tenv.TryFind l.value, IsKeyword l.value  with
            | Some( TEImFunc( Func(_, [], _) ) ), _ -> // label declared as a constant
                Checker tenv tstore (PReceive([], varList, P))
            | _, true -> raise <| KeywordDuplicationException l
            | _ -> raise <| NotDeclaredConstantException l 

        | PCmd(actuator, action, P) -> 
            if IsKeyword actuator.value then
                raise <| KeywordDuplicationException actuator

            if IsKeyword action.value then
                raise <| KeywordDuplicationException action

            let actuator = 
                match tstore.TryFind actuator.value with
                | Some( TSImActuator t ) -> t
                | Some _ -> raise <| NameDuplicationException actuator
                | _ when (tenv.ContainsKey actuator.value) -> 
                    raise <| NameDuplicationException actuator
                | _ -> raise <| NotDeclaredNameException actuator

            match tenv.TryFind action.value with
            // match of the actuator and the actuator associated to the action.
            | Some( TEImAction t ) when t = actuator -> 
                Checker tenv tstore P
            
            // action is an valid action but not the same actuator.
            | Some( TEImAction t ) when t <> actuator -> 
                raise <| UnexpectedActionException (action, actuator)

            // action is not an action in the env.
            | Some _ -> raise <| UnexpectedActionException (action, actuator) 

            //action is not an Action but name used into store
            | _ when (tstore.ContainsKey action.value) -> 
                raise <| UnexpectedActionException (action, actuator) 

            //action undeclared anywhere
            | _ -> raise <| NotDeclaredNameException action // the name doesn't exists

        | PCond(expr, P1, P2) ->
            match (Expression.Checker tenv tstore expr) with
            | ( TSImType( bt ), estore ) when bt = BoolType ->
                CheckerList tenv estore [P1;P2]
            | _ ->
                raise <| NotBooleanConditionException expr

    and 
        
        CheckerList (tenv : TEnv) (tstore : TStore) 
                            (procList : ProcessParseTree list) : TStore =

                     List.fold (Checker tenv) tstore procList

