namespace TypeChecker.Algorithms

module Sensor =

    open ParseTree
    open TypeChecker

    (*
       There is an assumption that the store contains the its sensor declaration,
       this because the declaration in the parse tree is in upper level.
    *)
    /// The function type check a sensor declaration with a given env and store.
    let rec Checker (tenv : TEnv) (tstore : TStore) (sensorTree : SensorParseTree) : TStore =
        match sensorTree with
        | SNil -> tstore
        | STau( S ) | SProbe(S) -> Checker tenv tstore S
        | SRec( h, S) ->
            if IsKeyword h.value then
                raise <| KeywordDuplicationException h

            if (tenv.ContainsKey h.value) || (tstore.ContainsKey h.value) then
                raise <| NameDuplicationException h

            Checker tenv (tstore.Add (h.value, REC)) S
        | SId (h) ->
            if IsKeyword h.value then
                raise <| KeywordDuplicationException h

            match tstore.TryFind h.value with
            | Some(REC) -> tstore
            | Some(_) -> raise <| NameDuplicationException h            
            | None -> 
                        if tenv.ContainsKey h.value then
                            raise <| NameDuplicationException h
                        else
                            raise <| NotDeclaredNameException h           
