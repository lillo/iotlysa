namespace TypeChecker.Algorithms

open SymbolTable
open TypeChecker

module SigDelaration =

    /// Type check a single signature declaration instance.
    let CheckerSingle (tenv : TEnv) (sigDel : ParseTree.SigDelaration) : TEnv =
        match sigDel with
        | ParseTree.TypeDecl name -> 
            if IsKeyword name.value then
                raise <| KeywordDuplicationException name

            if tenv.ContainsKey name.value then 
                raise <| NameDuplicationException name

            tenv.Add (name.value, TEImType <| Type name.value)
        
        | ParseTree.FuncDecl (name, args, ret) -> 
            // Only the name of the functors is checked not be a keyword.
            if IsKeyword name.value then 
                raise <| KeywordDuplicationException name

            (*
                There is no need to checking the reserving of argList e retSym
                since their names have to be types in the environment and 
                the types are on their own already checked if they are in the environment.
            *)

            (* The principal name has to be fresh. *)
            if tenv.ContainsKey name.value then
                raise <| NameDuplicationException name

            (* I can't check the reserving keyword issue since the primitive types
               them self are keyword. Any if I use e.g. process as ret type
               the code's raising an exception since the process type cannot
               be present*)
            (* The return name has to be a declared type. *)
            let retType = 
                match GetType tenv ret.value with
                    | Some t -> t
                    | _ -> raise <| TypeNotDeclaredException ret

            (* All the type's arguments have to be type declared. *)            
            let argsType = 
                List.map
                    (
                        fun (s : Symbol) ->
                            match GetType tenv s.value  with
                            | Some(t) -> t
                            | _ -> raise <| TypeNotDeclaredException s //found some no type
                    )
                    args

            tenv.Add (name.value, TEImFunc <| Func( name.value, argsType, retType ) )

        | ParseTree.ActuatorDecl( name, actions) -> 
            if IsKeyword name.value then 
                raise <| KeywordDuplicationException name

            (* The principal name has to be fresh. *)
            if tenv.ContainsKey name.value then
                raise <| NameDuplicationException name

            // ASSERT: the name is checked without including the actions!!!

            // All the inserts are safe at this point.
            let actionList = List.map (fun (actSym : Symbol) -> Action actSym.value) actions
            let newActuator = Actuator( name.value, set actionList)
            let envStartAdding = tenv.Add( name.value, TEImActuator newActuator)

            // We DON'T fold on (set actionList) because the doubles would have gone hidden!
            List.fold
                    ( fun e (act:Symbol) ->
                        if IsKeyword act.value then 
                            raise <| KeywordDuplicationException act
                        
                        // ensStartAdding includes the actuator name but not yet the actions
                        if envStartAdding.ContainsKey act.value then
                            raise <| NameDuplicationException act

                        // if there is a key, it must be a duplicate action..
                        // e change iterately adding one by one the new actions binds
                        if e.ContainsKey act.value then 
                            raise <| DoubleActionException act
                        
                        e.Add(act.value, TEImAction newActuator) 
                    )
                    envStartAdding
                    actions

    let Checker env sigs =
        List.fold CheckerSingle env sigs
        