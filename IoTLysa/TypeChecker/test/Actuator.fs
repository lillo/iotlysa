namespace TypeChecker.Test.Actuator

open FParsec
open NUnit.Framework
open SymbolTable
open LysaParser
open ParseTree
open TypeChecker

[<AutoOpenAttribute>]
module Utils =
    let mk = makeSymbol

    let (actuatorChecker, tenv, tstore) = 
        let text = 
            "
                type image_t
                func resizeImage(image_t) : image_t
                func green_screen : image_t
                actuator_type turnonoff {turnon, turnoff}
                actuator_type stereo { play, stop, rewind}
                actuator_type foofie { foo, fie}
            "
        let symTable = SymbolTable()
        let lysaParser = LysaParser(symTable)
        ignore ( symTable.AddSymbol "5" NumericId)
        match lysaParser.ParseOnString text with
            | Success((sigs, ndecls),_,_) ->
                let tenv = Algorithms.SigDelaration.Checker (makeTEnvStart(symTable)) sigs
                let gstate = GlobalState tenv ndecls

                let getActuator actuName =
                    match tenv.[actuName] with
                    | TEImActuator t -> t
                    | _ -> failwith "internal error taking the actuator value."

                let evalState = 
                    [   
                        "h", REC
                        "Temp", Sensor( NumericType )
                        "Lamp", TSImActuator( getActuator "turnonoff" )
                        "Cassa", TSImActuator( getActuator "stereo")
                    ] |>
                    List.fold (fun (s:TStore) p -> s.Add p) gstate
                    

                ((Algorithms.Actuator.Checker tenv evalState), tenv, evalState)
            | Failure( _ ) -> failwith "error during the parsing."

    let mkDelegate expr = (fun () -> ignore <| actuatorChecker expr)

    let Lamp = match tstore.["Lamp"] with 
                    | TSImActuator a -> a
                    | _ -> failwith "internal error test Actuator."
    
    let Cassa = match tstore.["Cassa"] with 
                    | TSImActuator a -> a
                    | _ -> failwith "internal error test Actuator."

module ANilATau =

    [<Test>]
    let ``ANil Lamp case`` () =
        let actuPair = (ANil, Lamp)
        Assert.AreEqual( actuatorChecker actuPair, tstore )

    [<Test>]
    let ``ATau.ANil Lamp case`` () =
        let actuPair = (ATau ANil, Lamp)
        Assert.AreEqual( actuatorChecker actuPair, tstore )

    [<Test>]
    let ``ANil Cassa case`` () =
        let actuPair = (ANil, Cassa)
        Assert.AreEqual( actuatorChecker actuPair, tstore )

    [<Test>]
    let ``ATau.ANil Cassa case`` () =
        let actuPair = (ATau ANil, Cassa)
        Assert.AreEqual( actuatorChecker actuPair, tstore )

module WaitFor =

    [<Test>]
    let ``ok WaitFor.ANil with no action at all`` () =
        let actuPair = (AWaitFor([], ANil ), Lamp)
        Assert.AreEqual( actuatorChecker actuPair, tstore )

    [<Test>]
    let ``WaitFor.ANil with a keyword action`` () =
        let actuPair = (AWaitFor([mk("turnon", ActId);mk("if", ActId)], ANil ), Lamp)
        Assert.That( mkDelegate actuPair,
                    Throws.TypeOf<KeywordDuplicationException>()    
                )

    [<Test>]
    let ``WaitFor.ANil with a keyword action 2`` () =
        let actuPair = (AWaitFor([mk("probe", ActId); mk("turnon", ActId);], ANil ), Lamp)
        Assert.That( mkDelegate actuPair,
                    Throws.TypeOf<KeywordDuplicationException>()    
                )

    [<Test>]
    let ``WaitFor.ANil with a keyword action 3`` () =
        let actuPair = (AWaitFor([mk("receive", ActId)], ANil ), Cassa)
        Assert.That( mkDelegate actuPair,
                    Throws.TypeOf<KeywordDuplicationException>()    
                )

    [<Test>]
    let ``WaitFor.ANil with a correct single action`` () =
        let actuPair = (AWaitFor([mk("play", ActId)], ANil ), Cassa)
        Assert.AreEqual( actuatorChecker actuPair, tstore )

    [<Test>]
    let ``WaitFor.ANil with a correct multiple actions`` () =
        let actuPair = (AWaitFor([mk("play", ActId); mk("stop", ActId); mk("rewind", ActId)], ANil )
                                , Cassa)
        Assert.AreEqual( actuatorChecker actuPair, tstore )

    [<Test>]
    let ``exception WaitFor.ANil with multiple actions but one of another actuator`` () =
        let actuPair = (AWaitFor([ mk("turnon", ActId); mk("stop", ActId); mk("rewind", ActId)], ANil )
                                , Cassa)
        Assert.That( mkDelegate actuPair,
                    Throws.TypeOf<UnexpectedActionException>()    
                )               
    [<Test>]
    let ``exception WaitFor.ANil with multiple actions but one of another actuator 2`` () =
        let actuPair = (AWaitFor([ mk("play", ActId); mk("turnoff", ActId); mk("rewind", ActId)], ANil )
                                , Cassa)
        Assert.That( mkDelegate actuPair,
                    Throws.TypeOf<UnexpectedActionException>()    
                )    

    [<Test>]
    let ``exception WaitFor.ANil with multiple actions but one as actuator name`` () =
        let actuPair = (AWaitFor([ mk("play", ActId); mk("stop", ActId); mk("stereo", ActId)], ANil )
                                , Cassa)
        Assert.That( mkDelegate actuPair,
                    Throws.TypeOf<UnexpectedActionException>()    
                )    


    [<Test>]
    let ``exception WaitFor.ANil with multiple actions but one as sensor name`` () =
        let actuPair = (AWaitFor([ mk("play", ActId); mk("stop", ActId); mk("Temp", ActId)], ANil )
                                , Cassa)
        Assert.That( mkDelegate actuPair,
                    Throws.TypeOf<UnexpectedActionException>()    
                )

    [<Test>]
    let ``exception WaitFor.ANil with multiple actions but one as function name`` () =
        let actuPair = (AWaitFor([ mk("play", ActId); mk("stop", ActId); mk("resizeImage", ActId)], ANil )
                                , Cassa)
        Assert.That( mkDelegate actuPair,
                    Throws.TypeOf<UnexpectedActionException>()    
                )

    [<Test>]
    let ``exception WaitFor.ANil with multiple actions but one as not declared name`` () =
        let actuPair = (AWaitFor([ mk("play", ActId); mk("stop", ActId); mk("foo_unknown", ActId)], ANil )
                                , Cassa)
        Assert.That( mkDelegate actuPair,
                    Throws.TypeOf<UnexpectedActionException>()    
                )

module ARec =

    [<Test>]
    let ``correct the typical structure of an actuator`` () =
        let i = mk("i", RecId)
        let actuPair =  ARec(
                            i, 
                            AWaitFor( 
                                [mk("turnon", ActId); mk("turnoff", ActId)] , AId i
                            )
                        ) , Lamp
        Assert.AreEqual( actuatorChecker actuPair, tstore.Add("i", REC) )

    [<Test>]
    let ``try to create a rec variable as kewyord`` () =
        let var = mk("to", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<KeywordDuplicationException>())

    [<Test>]
    let ``try to create a rec variable as names in the store`` () =
        let var = mk("h", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to create a rec variable as names in the store 2`` () =
        let var = mk("Temp", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to create a rec variable as names in the store 3`` () =
        let var = mk("Lamp", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to create a rec variable as names in the store 4`` () =
        let var = mk("Cassa", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to create as names in the env`` () =
        let var = mk("foofie", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to create as names in the env 2`` () =
        let var = mk("image_t", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to create as names in the env 3`` () =
        let var = mk("foo", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to create as names in the env 4`` () =
        let var = mk("turnon", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to create as names in the env 5`` () =
        let var = mk("resizeImage", RecId)
        let actuPair = ARec( var, AId var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())


module AId =

    [<Test>]
    let ``try to recId variable as kewyord`` () =
        let var = mk("to", RecId)
        let actuPair = AId( var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<KeywordDuplicationException>())

    [<Test>]
    let ``try to recId variable in the store not rec`` () =
        let var = mk("Temp", RecId)
        let actuPair = AId( var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to recId variable with a name used in the env`` () =
        let var = mk("image_t", RecId)
        let actuPair = AId( var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``try to recId variable not declaread anywhere`` () =
        let var = mk("foo_boh", RecId)
        let actuPair = AId( var), Lamp
        Assert.That( mkDelegate actuPair
                    , Throws.TypeOf<NotDeclaredNameException>())

    [<Test>]
    let ``correc RecId`` () =
        let var = mk("h", RecId)
        let actuPair = AId( var), Lamp
        Assert.AreEqual( actuatorChecker actuPair, tstore)
