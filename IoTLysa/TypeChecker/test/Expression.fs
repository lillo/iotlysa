namespace TypeChecker.Test.Expression

open FParsec
open NUnit.Framework
open SymbolTable
open ParseTree
open LysaParser
open TypeChecker

[<AutoOpenAttribute>]
module Utils =
    let mk = makeSymbol

    let undef1 = TSImTypeVar( freshTypeVarGenerator() )
    let undef2 = TSImTypeVar( freshTypeVarGenerator() )

    let (exprChecker, tstore) = 
        let text = sprintf
                        "
                            type image_t
                            func resizeImage(image_t) : image_t
                            func green_screen : image_t
                            actuator_type turnonoff {turnon, turnoff}
                            func sumFloat(%s, %s):%s
                        " NumericName NumericName NumericName

        let symTable = SymbolTable()
        ignore ( symTable.AddSymbol "-23" NumericId; symTable.AddSymbol "24.5" NumericId )
        let lysaParser = LysaParser(symTable)
        match lysaParser.ParseOnString text with
            | Success((sigs, ndecls),_,_) ->
                let tenv = Algorithms.SigDelaration.Checker (makeTEnvStart(symTable)) sigs
                let gstate = GlobalState tenv ndecls

                let evalState = 
                    [               
                        "x", TSImType( NumericType )
                        "y", TSImType( NumericType )
                        "undef1", undef1
                        "undef2", undef2
                        "CopyUndef2", undef2
                        "z", TSImType( NumericType )
                        "Temp", Sensor( NumericType )
                        "i", REC
                        "node1", NODEID
                        "flag", TSImType( BoolType )
                    ] |>
                    List.fold (fun (s:TStore) p -> s.Add p) gstate
                    

                ((Algorithms.Expression.Checker tenv evalState), evalState)
            | Failure( _ ) -> failwith "error during the parsing."


    let delegateExprChecker expr = (fun () -> ignore <| exprChecker expr)

    let numericResult = TSImType(NumericType )

    let boolResult = TSImType( BoolType )
    let imagetResult = TSImType(Type "image_t")

module EqualOperator =

    let eq = mk("=", FuncId)
    let x = mk("x", VarId)
    let y = mk("y", VarId)

    [<Test>]
    let ``x = y, two numeric values`` () = 
        let expr = App( eq, [App(x, []); App(y, [])] )
        Assert.AreEqual( exprChecker expr, (boolResult, tstore) )

    [<Test>]
    let ``true = (x=y) --> ok`` () = 
        let expr = App( eq, [
                             App(mk(TrueName, FuncId), []); 
                             App( eq, [App(x, []); App(y, [])] )
                            ] )
        Assert.AreEqual( exprChecker expr, (boolResult, tstore) )

    [<Test>]
    let ``x = flag, different types`` () = 
        let expr = App( eq, [App(x, []); App(mk("flag", VarId), [])] )
        Assert.That( delegateExprChecker expr
                    , Throws.TypeOf<ArgumentsFunctionMatchException>())


    [<Test>]
    let ``Temp = flag, different types, sensor case`` () = 
        let expr = App( eq, [App(mk("Temp", VarId), []); App(mk("flag", VarId), [])] )
        Assert.That( delegateExprChecker expr
                    , Throws.TypeOf<ArgumentsFunctionMatchException>())

    [<Test>]
    let ``flag = Temp, different types, sensor case`` () = 
        let expr = App( eq, [App(mk("flag", VarId), []); App(mk("Temp", VarId), [])] )
        Assert.That( delegateExprChecker expr
                    , Throws.TypeOf<ArgumentsFunctionMatchException>())

    [<Test>]
    let ``= with 3 arguments`` () = 
        let expr = App( eq, [App(mk("Temp", VarId), []); App(mk("Temp", VarId), []); App(mk("Temp", VarId), []);] )
        Assert.That( delegateExprChecker expr
                    , Throws.TypeOf<ArgumentsFunctionMatchException>())

    [<Test>]
    let ``x = undef2, comparing with a generic type`` () = 
        let expr = App( eq, [App(x, []); App(mk("undef2", VarId), [])] )
        Assert.AreEqual( exprChecker expr, 
                        (
                            boolResult,
                            tstore.Add("undef2", TSImType( NumericType ))
                                  .Add("CopyUndef2", TSImType( NumericType ))
                        )
        )

    [<Test>]
    let ``x = Temp, ok`` () = 
        let expr = App( eq, [App(x, []); App(mk("Temp", VarId), [])] )
        Assert.AreEqual( exprChecker expr, 
                        (
                            boolResult,
                            tstore
                        )
        )


    [<Test>]
    let ``Temp = x, ok`` () = 
        let expr = App( eq, [App(mk("Temp", VarId), []); App(x, [])] )
        Assert.AreEqual( exprChecker expr, 
                        (
                            boolResult,
                            tstore
                        )
        )

    [<Test>]
    let ``undef2 = x, comparing with a generic type`` () = 
        let expr = App( eq, [App(mk("undef2", VarId), []); App(x, [])] )
        Assert.AreEqual( exprChecker expr, 
                        (
                            boolResult,
                            tstore.Add("undef2", TSImType( NumericType ))
                                  .Add("CopyUndef2", TSImType( NumericType ))
                        )
        )

    [<Test>]
    let ``undef1=undef2, comparing between two generic type`` () = 
        let expr = App( eq, [App(mk("undef1", VarId), []); App(mk("undef2", VarId), [])] )
        let (eval, estore) = exprChecker expr
        Assert.AreEqual( (eval, estore), 
                        (
                            boolResult,
                            tstore.Add("undef1", estore.["undef2"])
                                  .Add("undef2", estore.["undef1"])
                                  .Add("CopyUndef2", estore.["undef1"])
                        )
        )
    

module Foo =

    [<Test>]
    let ``sum of two constant value as number: (-23+24.5)`` () =
        let expr =
            App(
                mk("+", FuncId),
                [
                    App(mk("-23", FuncId), []);
                    App(mk("24.5", FuncId), [])
                ]
            )
        Assert.AreEqual(
            exprChecker expr
            , (numericResult, tstore)
        )

    [<Test>]
    let ``subtraction of constant and sensor value as number: (-23-Temp)`` () =
        let expr =
            App(
                mk("-", FuncId),
                [
                    App(mk("-23", FuncId), []);
                    App(mk("Temp", SenId), [])
                ]
            )
        Assert.AreEqual(
            exprChecker expr
            , (numericResult, tstore)
        )

    [<Test>]
    let ``opposite value sensor -Temp`` () =
        let expr =
            App(
                mk("--", FuncId),
                [
                    App(mk("Temp", SenId), [])
                ]
            )
        Assert.AreEqual(
            exprChecker expr
            , (numericResult, tstore)
        )


    [<Test>]
    let ``variable in the store`` () =
        Assert.AreEqual( 
            exprChecker <| App( mk("x",VarId), []),
            (numericResult, tstore)
        )

    [<Test>]
    let ``sensor variable in the store`` () =
        Assert.AreEqual( 
            exprChecker <| App( mk("Temp", SenId), []),
            (numericResult, tstore) )


    [<Test>]
    let ``REC value is not evaluable`` () =
        Assert.That( 
            delegateExprChecker <| App(mk("i",RecId), []) ,
            Throws.TypeOf<VariableEvaluatingException>()
        ) 

    [<Test>]
    let ``NODEID value is not evaluable`` () =
        Assert.That( 
            delegateExprChecker <| App(mk("node1", NodeId), []) ,
            Throws.TypeOf<VariableEvaluatingException>()
        ) 

    [<Test>]
    let ``FUNC value is not evaluable`` () =
        Assert.That( 
            delegateExprChecker <| App(mk("+",RecId), []) ,
            Throws.TypeOf<NotDeclaredConstantException>()
        ) 

    [<Test>]
    let ``ACTUATOR value is not evaluable`` () =
        Assert.That( 
            delegateExprChecker <| App(mk("turnonoff", NodeId), []) ,
            Throws.TypeOf<NotDeclaredConstantException>()
        ) 

    [<Test>]
    let ``correct usage of a constant`` () =
        Assert.AreEqual( 
            exprChecker <| App( mk("green_screen", FuncId),[])
            , (imagetResult, tstore)
        ) 


    [<Test>]
    let ``trial to use a function as a constant.`` () =
        Assert.That( 
            delegateExprChecker <| App( mk("resizeImage", FuncId),[]) ,
            Throws.TypeOf<NotDeclaredConstantException>()
        ) 


    [<Test>]
    let ``the variable does not exists anywhere.`` () =
        Assert.That( 
            delegateExprChecker <| App(mk("IdontExist", FuncId), []) ,
            Throws.TypeOf<NotDeclaredNameException>()
        ) 

    [<Test>]
    let ``call to function that not exists anywhere with some arguments`` () =
        Assert.That( 
            delegateExprChecker <| 
                App(mk("IdontExist", FuncId), [ App(mk("x", VarId),[]); App(mk("Temp", SenId),[])])
            ,Throws.TypeOf<NotDeclaredFunctionException>()
        )  


    [<Test>]
    let ``exception on sum of a number variabile and a number sensor`` () =
        let x = App(mk("x", VarId), [])
        let temp = App(mk("Temp", SenId), [])
        let expr = App(mk("+", FuncId),[x; temp])
        Assert.AreEqual(
            exprChecker expr
            , (numericResult, tstore)
        )

    [<Test>]
    let ``sum of two number variabile`` () =
        let x = App(mk("x", VarId), [])
        let temp = App(mk("y", VarId), [])
        let expr = App(mk("+", FuncId),[temp; x])
        Assert.AreEqual(
            exprChecker expr
            , (numericResult, tstore)
        )

    [<Test>]
    let ``call of not function with number argument, so exception`` () =
        let expr = App(mk("not", FuncId), [App(mk("x", VarId),[])] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<ArgumentsFunctionMatchException>()
        )

    [<Test>]
    let ``call of not function with two arguments, so exception`` () =
        let flag = App(mk("flag", SenId), [])
        let trueExpr = App(mk(TrueName, FuncId), [])
        let expr = App(mk("not", FuncId), [flag; trueExpr] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<ArgumentsFunctionMatchException>()
        )

    [<Test>]
    let ``correct call of function not(true)`` () =
        let trueExpr = App(mk(TrueName, FuncId), [])
        let expr = App(mk("not", FuncId), [trueExpr] )
        Assert.AreEqual(
            exprChecker expr
            , (boolResult, tstore)
        )

    [<Test>]
    let ``correct call of function AND(false, flag)`` () =
        let flag = App(mk("flag", SenId), [])
        let trueExpr = App(mk(FalseName, FuncId), [])
        let expr = App(mk("&&", FuncId), [flag; trueExpr] )

        Assert.AreEqual(
            exprChecker expr
            , (boolResult, tstore)
        )

    [<Test>]
    let ``correct call of function AND( true, NOT(flag) OR FALSE)`` () =
        let trueExpr = App(mk(TrueName, VarId), [])
        let falseExpr = App(mk(FalseName, VarId), [])
        let notFlag = App(mk("not", FuncId), [App(mk("flag", FuncId), [])] )
        let notFlagOrFalse = App(mk("||", FuncId), [notFlag; falseExpr])
        let expr = App(mk("&&", FuncId), [trueExpr; notFlagOrFalse] )
        Assert.AreEqual(
            exprChecker expr
            , (boolResult, tstore)
        )


    [<Test>]
    let ``exception on AND(x,flag) since x is a number variable`` () =
        let expr = App(mk("&&", FuncId), [ App(mk("x", VarId), [] ); App(mk("flag", VarId),[])  ] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<ArgumentsFunctionMatchException>()    
        )

    [<Test>]
    let ``trial to call an actuator as a funcion`` () =
        let expr = App(mk("turnonoff", FuncId), [ App(mk("x", VarId), [] ); App(mk("flag", VarId),[])  ] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<NotDeclaredFunctionException>()    
        )


    
    [<Test>]
    let ``trial to call AND(flag, image_t), where image_t is custom type`` () =
        let expr = App(mk("&&", FuncId), [ App(mk("flag", VarId), [] ); App(mk("image_t", VarId),[])  ] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<NotDeclaredConstantException>()    
        )

    [<Test>]
    let ``trial to call image_t type as a function with arguments`` () =

        let expr = App( mk("image_t", FuncId), [App(mk("x", VarId), [] )] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<NotDeclaredFunctionException>()    
        )

    [<Test>]
    let ``trial to call a function with arguments doesn't exists`` () =
        let expr = App( mk("fie", FuncId), [App(mk("x", VarId), [] )] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<NotDeclaredFunctionException>()    
        )

    [<Test>]
    let ``trial to call turno action as a function with arguments`` () =
        let expr = App( mk("turnon", FuncId), [App(mk("x", VarId), [] )] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<NotDeclaredFunctionException>()    
        )

    [<Test>]
    let ``trial to call turno action as a constant`` () =
        let expr = App( mk("turnon", FuncId), [ ] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<NotDeclaredConstantException>()    
        )

    [<Test>]
    let ``trial to evaluate a actuator name`` () =
        let expr = App( mk("turnonoff", FuncId), [ ] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<NotDeclaredConstantException>()    
        )

    [<Test>]
    let ``trial to evaluate an action name`` () =
        let expr = App( mk("turnoff", FuncId), [ ] )
        Assert.That(
            delegateExprChecker expr
            , Throws.TypeOf<NotDeclaredConstantException>()    
        )


module Infering =

    [<Test>]
    let ``infer from custom function resizeImage(undef1)`` () =
        let expr = App(mk("resizeImage", FuncId), [App(mk("undef1", VarId),[])] )
        Assert.AreEqual(
            exprChecker expr
            , (imagetResult, tstore.Add("undef1", imagetResult))
        ) 

    [<Test>]
    let ``infer of NOT(undef1)`` () =
        let expr = App(mk("not", FuncId), [App(mk("undef1", VarId),[])] )
        Assert.AreEqual(
            exprChecker expr
            , (boolResult, tstore.Add("undef1", boolResult))
        )


    [<Test>]
    let ``infer of undef1=undef2 into two number values`` () =
        let expr = App(mk("=", FuncId), [App(mk("undef1", VarId),[]); App(mk("undef2", VarId),[])] )
        //By implementation the first generic value overrides the other one
        Assert.AreEqual(
            exprChecker expr
            , (boolResult, tstore.Add("undef1", undef2))
        )

    
    [<Test>]
    let ``impossibile infer ((x+undef1) = x) && undef1, x is a numeric `` () =
        let expr = App( mk("&&", FuncId), 
                        [
                            App( mk("=", FuncId),
                                [
                                    App( mk("+",FuncId), 
                                        [
                                            App( mk("x", VarId), []);
                                            App(mk("undef1", VarId),[])
                                        ];
                                    );
                                    App( mk("x", VarId), []) 
                                ]
                            )
                            App(mk("undef1", VarId),[])
                        ]
                    )
        Assert.That( 
            delegateExprChecker expr ,
            Throws.TypeOf<ArgumentsFunctionMatchException>()
        ) 

    [<Test>]
    let ``infer (x+undef1)+undef1 in numeric type value`` () =
        let expr =
            App( mk("+",FuncId), 
                [
                    App( mk("+",FuncId), 
                        [
                            App( mk("x", VarId), []);
                            App(mk("undef1", VarId),[])
                        ];
                    );
                    App(mk("undef1", VarId),[])
                ]
            )
        Assert.AreEqual(
            exprChecker expr,
            (numericResult, tstore.Add("undef1", numericResult))
        )

    [<Test>]
    let ``infer undef1+(x+undef1) in numeric type value`` () =
        let expr =
            App( mk("+",FuncId), 
                [
                    App(mk("undef1", VarId),[]);
                    App( mk("+",FuncId), 
                        [
                            App( mk("x", VarId), []);
                            App(mk("undef1", VarId),[])
                        ];
                    )
                ]
            )
        Assert.AreEqual(
            exprChecker expr,
            (numericResult, tstore.Add("undef1", numericResult))
        )

    [<Test>]
    let ``infer ((x+y) = undef1) && undef2, x,y is a numeric, undef1 as numeric, undef2 as bool `` () =
        let expr = App( mk("&&", FuncId), 
                        [
                            App( mk("=", FuncId),
                                [
                                    App( mk("+",FuncId), 
                                        [
                                            App( mk("x", VarId), []);
                                            App(mk("y", VarId),[])
                                        ];
                                    );
                                    App( mk("undef1", VarId), []) 
                                ]
                            )
                            App(mk("undef2", VarId),[])
                        ]
                    )
        Assert.AreEqual(
            exprChecker expr
            , (boolResult, tstore.Add("undef1", numericResult)
                                 .Add("undef2", boolResult)
                                 .Add("CopyUndef2", boolResult))
        ) 


    [<Test>]
    let ``evaluating undef1 in its value itself`` () =
        let expr = App( mk("undef1", VarId), [] )
        Assert.AreEqual(
            exprChecker expr
            , (undef1, tstore)
        )


    [<Test>]
    let ``undef1 and undef2 are different general type`` () =
        Assert.AreNotEqual( undef1, undef2 )

