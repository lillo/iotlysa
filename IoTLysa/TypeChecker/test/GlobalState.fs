namespace TypeChecker.Test.GlobalState

open FParsec
open NUnit.Framework
open LysaParser
open SymbolTable
open TypeChecker

    [<AutoOpenAttribute>]
    module Utils =

        let printfnTStore (store:TStore) =
            store
            |> Map.iter ( fun k e -> printfn "%s -- %O" k e)

        let checkContent text expResult = 
            let symTable = SymbolTable()
            let lysaParser = LysaParser(symTable)


            match lysaParser.ParseOnString text with
            | Success ((sigs, ndecls),_,_) -> 
                let env = Algorithms.SigDelaration.Checker (makeTEnvStart(symTable)) sigs
                let retStore = GlobalState env ndecls

                printf "%A" retStore

                expResult |> 
                    List.forall (fun (symType, ide) -> retStore.TryFind ide = Some(symType) )

            | _ -> failwith "parsing shouldn't fail."


        let checkLenght text len =
            let symTable = SymbolTable()
            let lysaParser = LysaParser(symTable)


            match lysaParser.ParseOnString text with
            | Success ((sigs, ndecls),_,_) -> 
                let env = Algorithms.SigDelaration.Checker (makeTEnvStart(symTable)) sigs
                let retStore = GlobalState env ndecls

                printfn "the size of the store was %d, expected %d."
                                retStore.Count len

                retStore.Count = len
            | _ -> failwith "parsing shouldn't fail."



    module Foo =
        let private emptyProgram = ""
        let private expectedEmptyProgram = []

        [<Test>]
        let ``empty program, check content`` () =
            Assert.IsTrue (checkContent emptyProgram expectedEmptyProgram)

        [<Test>]
        let ``empty program, check length content`` () =
            Assert.IsTrue ( checkLenght emptyProgram expectedEmptyProgram.Length)

        [<Test>]
        let ``exception for usage NodeID name declared in the env section`` () =
            Assert.That( 
                (fun () ->     
                    checkContent 
                        ("
                            type image_t
                            node image_t =
	                            sensor Scp : image_t =  rec h. tau; probe; h 
                         ") 
                        [NODEID, "image_t"]
                ) >> ignore
                , Throws.TypeOf<NameDuplicationException>())


        [<Test>]
        let ``exception for a duplicate node-id`` () =
            Assert.That( 
                (fun () ->     
                    checkContent 
                        ("
                            node Node1 =
	                            sensor Scp : image_t =  rec h. tau; probe; h 
                            
                            node Node2 =
	                            sensor Scp : image_t =  rec h. tau; probe; h 

                            node Node1 =
	                            sensor Scp : image_t =  rec h. tau; probe; h 
                         ") 
                        [(NODEID, "Node1"); (NODEID, "Node2")]
                ) >> ignore
                , Throws.TypeOf<NameDuplicationException>())
        
        
        [<Test>]
        let ``exception for usage NodeID name declared as keyword numeric`` () =
            Assert.That( 
                (fun () ->     
                    checkContent 
                        (sprintf "node %s =
	                              sensor Scp : image_t =  rec h. tau; probe; h 
                         " NumericName) 
                        [NODEID, NumericName]
                ) >> ignore
                , Throws.TypeOf<KeywordDuplicationException>())

    module Example1 = 
       
        let private text = 
            sprintf "type image_t
                    func noiseRed(image_t) : image_t
                    func car : image_t
                    func err : %s
                    func is_a_car(image_t) : %s

                    actuator_type swict_t {turnon, turnoff} 


                    node N_cp  = 

                        sensor Scp : image_t =  rec h. tau; probe; h 

                        process = 
                    	rec h. 
                               z := Scp;            
                               zp := noiseRed(z);    
                               send (zp) to [N_la]; 
                               h
                      
                     node N_la = 
                        process = 
                            rec h.
                                receive (;x);
                                send (car, x) to [N_cp];
                                h

                    " BoolName BoolName

        let private expectedContent = [ 
            NODEID, "N_cp"
            NODEID, "N_la"
        ]


        [<Test>]
        let ``test the content`` () =
            Assert.IsTrue ( checkContent text expectedContent)

        [<Test>]
        let ``test the length content`` () =
            Assert.IsTrue ( checkLenght text expectedContent.Length)


    module TavernFix = 
       
        let private text = 
            "
            node Manager = 
                process = 
                    rec h.
                    recv(innl; givemelist);
                    snd (req, sales) to [SuperMrk];
                    recv(res; offerOfDay);
                    wine := choice(offerOfDay, w);
                    if i then
                        snd(list, nil, wine) to [Innkeeper];
                        h
                    else
                        food := choice(offerOfDay, f);
                        snd (list, food, wine) to [Innkeeper];
                        h
                        
                process = 
                    rec h.
                    recv(inni; goout);
                    i := ff;
                    snd(content,now) to [Fridge, WineCellar];
                    recv(fridge; x);
                    y := x;
                    recv(cellar; w);
                    snd(menu,today) to [RecipeBook];
                    recv(menu; r);
                    y := select(x,r);
                    i := (y = x);
                    h


            node SuperMrk =
                process = 
                    rec h.
                    recv(req; sales);
                    tau;
                    snd(res,offers) to [Manager];
                    h
                    
                process = 
                    rec h.
                    recv(buy; f);
                    tau;
                    h
                    
            node Innkeeper =
                process = 
                    rec h.
                    snd(inni, goout) to [Manager];
                    snd(innl, givemelist) to [Manager];
                    recv(list; f, w);
                    snd (buy, w) to [WineHouse];
                    snd (buy, f) to [SuperMrk];
                    tau;
                    h
                    
            node WineHouse =
                process = 
                    rec h.
                    recv(buy; w);
                    tau;
                    h
                    
            node Fridge =
                process = 
                    rec h.
                    recv(content; now);
                    tau;
                    snd(fridge, c) to [Manager];
                    h        
                    
            node WineCellar =
                process = 
                    rec h.
                    recv(content;now);
                    tau;
                    snd(cellar, w) to [Manager];
                    h                
                    
            node RecipeBook =
                process = 
                    rec h.
                    recv(menu; today);
                    tau;
                    snd(menu, r) to [Manager];
                    h                    
            "

        let private expectedContentTavernFix = [ 
            NODEID, "SuperMrk"
            NODEID, "Innkeeper"
            NODEID, "Fridge"
            NODEID, "WineCellar"
            NODEID, "RecipeBook"
            NODEID, "Manager"
            NODEID, "WineHouse"
        ]


        [<Test>]
        let ``test the content`` () =
            Assert.IsTrue ( checkContent text expectedContentTavernFix )

        [<Test>]
        let ``test the length content`` () =
            Assert.IsTrue ( checkLenght text expectedContentTavernFix.Length )
   

    module AvgNewCommNoSend =
        
        let private text = 
            sprintf "
                        func quito       : %s 
                        func tempo       : %s
                        func on         : %s
                        func off        : %s
                        func tthreshold : %s
                        func bthreshold : %s 

                        actuator_type onoff_t {turnon, turnoff} 

                        node Thermometer1 = 
                          sensor Temp1 : %s = rec h. tau; probe; h
                          sensor Battery : %s = rec h. tau; probe; h

                          process =
                            i := 0;      
                            rec h.
                              if bthreshold < Battery then        
                                recv(temp;x);     
                                t := Temp1;   
                                tp := t + x;      
                                mt := tp / 2; 
                                i := i + 1;     
                                if i = 10 then
                                  snd (temp,mt) to [Thermostat,Thermometer2];      
                                  recv(ack;j);     
                                  i := 0;
                                  h
                                else
                                snd (temp,mt) to [Thermometer2];  
                                h
                              else
                                snd (quit,0) to [Thermostat]; 
                                nil 

                          process = 
                            rec h.
                              recv(temp;s);
                              mt := s;
                              h 



                        node Thermometer2 = 
                          sensor Temp2 : %s = rec h. tau; probe; h

                          process = 
                            mt := 0;
                            rec h.
                              snd (temp,mt) to [Thermometer1];
                              recv(temp;x);
                              t := Temp2;
                              tp := t;
                              mt := tp / 2;
                              h 


                        node Thermostat = 
                             actuator ACon_off : onoff_t =  rec h. wait_for(turnon, turnoff); h 

                             process = 
                                 rec h. 
                                    switch {
                                      recv(temp;x); 
                                      if tthreshold < x then 
                                          @ACon_off.turnon;
                                          snd (ack,on) to [Thermometer1]; 
                                          h
                                      else
                                          @ACon_off.turnoff;
                                          snd (ack,off) to [Thermometer1]; 
                                          h
                                    |
                                      recv(quit;i); 
                                      nil
                                    } 
                            "
                NumericName NumericName BoolName BoolName NumericName NumericName
                NumericName NumericName NumericName

        let private expectedContent = [ 
            NODEID, "Thermostat"
            NODEID, "Thermometer1"
            NODEID, "Thermometer2"
        ]


        [<Test>]
        let ``test the content`` () =
            Assert.IsTrue ( checkContent text expectedContent )

        [<Test>]
        let ``test the length content`` () =
            Assert.IsTrue ( checkLenght text expectedContent.Length )
           

