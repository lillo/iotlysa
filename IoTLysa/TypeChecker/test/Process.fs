namespace TypeChecker.Test.Process

open FParsec
open NUnit.Framework
open SymbolTable
open ParseTree
open LysaParser
open TypeChecker
open TypeChecker.Test.Expression.Utils

[<AutoOpenAttribute>]
module Utils =
    let mk = makeSymbol

    let undef1 = TSImTypeVar( freshTypeVarGenerator() )
    let undef2 = TSImTypeVar( freshTypeVarGenerator() )

    let (procChecker, tstore) = 
        let text = 
            "
                type image_t
                func resizeImage(image_t) : image_t
                func green_screen : image_t
                actuator_type turnonoff {turnon, turnoff}
                actuator_type stereo { play, stop}
            "
        let symTable = SymbolTable()
        let lysaParser = LysaParser(symTable)
        ignore ( symTable.AddSymbol "5" NumericId)
        match lysaParser.ParseOnString text with
            | Success((sigs, ndecls),_,_) ->
                let tenv = Algorithms.SigDelaration.Checker (makeTEnvStart(symTable)) sigs
                let gstate = GlobalState tenv ndecls

                let getActuator actuName =
                    match tenv.[actuName] with
                    | TEImActuator t -> t
                    | _ -> failwith "internal error taking the actuator value."

                let evalState = 
                    [               
                        "x", TSImType( NumericType )
                        "y", TSImType( NumericType )
                        "undef1", undef1
                        "undef2", undef2
                        "CopyUndef2", undef2
                        "z", TSImType( NumericType )
                        "Temp", Sensor( NumericType )
                        "i", REC
                        "photo", TSImType( Type "image_t" )
                        "Client", NODEID
                        "Server", NODEID
                        "flag", TSImType( BoolType )
                        "Lamp", TSImActuator( getActuator "turnonoff" )
                        "Cassa", TSImActuator( getActuator "stereo")
                    ] |>
                    List.fold (fun (s:TStore) p -> s.Add p) gstate
                    

                ((Algorithms.Process.Checker tenv evalState), evalState)
            | Failure( _ ) -> failwith "error during the parsing."


    let mkDelegate expr = (fun () -> ignore <| procChecker expr)

    let sensorFloatResult = Sensor( NumericType )
    let boolResult = TSImType( BoolType )
    let imagetResult = TSImType( Type "image_t" )


module PNilTau =

    [<Test>]
    let ``PNil case`` () =
        let proc = PNil
        Assert.AreEqual(
            procChecker proc
            , tstore
        )


    [<Test>]
    let ``PTau.PNil case`` () =
        let proc = PTau(PNil)
        Assert.AreEqual(
            procChecker proc
            , tstore
        )


module PRec = 

    [<Test>]
    let ``exception for PRec(i,P) and store(i)=REC`` () =
        let proc = PRec(mk("i", RecId), PNil)
        
        Assert.AreEqual( procChecker proc, tstore)


    [<Test>]
    let ``exception for PRec(flag,P) and store(flag)=bool`` () =
        let proc = PRec(mk("flag", RecId), PNil)
        
        Assert.That(mkDelegate proc
                    ,Throws.TypeOf<NameDuplicationException>())


    [<Test>]
    let ``exception for PRec(image_t,P) and env(image_t) is a type`` () =
        let proc = PRec(mk("image_t", RecId), PNil)
        
        Assert.That(mkDelegate proc
                    ,Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``exception for PRec(type,P) and type is a keyword`` () =
        let proc = PRec(mk("type", RecId), PNil)
        
        Assert.That(mkDelegate proc
                    ,Throws.TypeOf<KeywordDuplicationException>())

    [<Test>]
    let ``PRec(h,P) wit h name not used`` () =
        let proc = PRec(mk("h", RecId), PNil)
        
        Assert.AreEqual( procChecker proc
                    ,tstore.Add("h", REC))


module PId = 
    [<Test>]
    let ``exception PId(process) usage keyword`` () =
        let proc = PId(mk("process", RecId))
        Assert.That(mkDelegate proc
            ,Throws.TypeOf<KeywordDuplicationException>())


    [<Test>]
    let ``exception PId(h) h not exists`` () =
        let proc = PId(mk("h", RecId))
        Assert.That(mkDelegate proc
            ,Throws.TypeOf<NotDeclaredNameException>())

    [<Test>]
    let ``exception PId(green_screen) name exists in the env`` () =
        let proc = PId(mk("green_screen", RecId))
        Assert.That(mkDelegate proc
            ,Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``exception PId(undef1) not a REC value`` () =
        let proc = PId(mk("undef1", RecId))
        Assert.That(mkDelegate proc
            ,Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``Ok --> PId(i) store(i)=REC`` () =
        let proc = PId(mk("i", RecId))
        Assert.AreEqual(procChecker proc
            ,tstore)


module PSwitch =

    [<Test>]
    let ``empty switch`` () =
        let proc = PSwitch([])

        Assert.AreEqual( procChecker proc, tstore)


    [<Test>]
    let ``first case make an assignment to new name, used in the second one`` () =
        let proc = PSwitch(
                    [
                        PAssign( mk("foo_var", VarId),
                                App(mk("green_screen", VarId),[]),
                                PNil 
                        );
                        PAssign( mk("photo", VarId),
                                App(mk("foo_var", VarId),[]),
                                PNil 
                        );
                    ])

        Assert.AreEqual( procChecker proc, tstore.Add("foo_var", imagetResult))

    [<Test>]
    let ``branch make assignment for the same type value`` () =
        let proc = PSwitch(
                    [
                        PAssign( mk("foo_var", VarId),
                                App(mk(TrueName, VarId),[]),
                                PNil 
                        );
                        PAssign( mk("foo_var", VarId),
                                App(mk(FalseName, VarId),[]),
                                PNil 
                        );
                    ])

        Assert.AreEqual( procChecker proc, tstore.Add("foo_var", boolResult))

    [<Test>]
    let ``usage of a name with different types between two branch`` () =
        let proc = PSwitch(
                    [
                        PAssign( mk("foo_var", VarId),
                                App(mk("green_screen", VarId),[]),
                                PNil 
                        );
                        PAssign( mk("foo_var", VarId),
                                App(mk(TrueName, VarId),[]),
                                PNil 
                        );
                    ])

        Assert.That( mkDelegate proc
                    , Throws.TypeOf< UnexpectedAssignmentException>())


module PAssign = 

    [<Test>]
    let ``simple assignment`` () =
        let proc = PAssign( mk("foo_var", VarId),
                            App(mk("green_screen", VarId),[]),
                            PNil )
        Assert.AreEqual(
            procChecker proc
            , tstore.Add("foo_var", imagetResult)
        )


    [<Test>]
    let ``undef2 = undef1, change also of CopyUndef2`` () =
        let proc = PAssign(
                        mk("undef2", VarId),
                        App(mk("undef1", VarId), []),
                        PNil
                    )
        Assert.AreEqual(
            procChecker proc,
            tstore.Add("undef2", undef1).Add("CopyUndef2", undef1)
        )

    [<Test>]
    let ``can't assign REC values`` () =
        let proc = PAssign(mk("foo_new", VarId), App(mk("i", VarId),[]), PNil)
        Assert.That( mkDelegate proc
                    , Throws.TypeOf<VariableEvaluatingException>())

    [<Test>]
    let ``can't assign image_t to numeric variable`` () =
        let proc = PAssign(mk("x", VarId), App(mk("photo", VarId),[]), PNil)
        Assert.That( mkDelegate proc
                    , Throws.TypeOf<UnexpectedAssignmentException>())


    [<Test>]
    let ``can't assign a value to a sensor`` () =
        let proc = PAssign(mk("Temp", VarId), App(mk("x", VarId),[]), PNil)
        Assert.That( mkDelegate proc
                    , Throws.TypeOf<UnexpectedAssignmentException>()) 


    [<Test>]
    let ``can't assign a value to rec variable`` () =
        let proc = PAssign(mk("i", VarId), App(mk("i", VarId),[]), PNil)
        Assert.That( mkDelegate proc
                    , Throws.TypeOf<VariableEvaluatingException>())  

    [<Test>]
    let ``assign to h a numeric value but then try to recurse in it`` () =
        let proc = PAssign(mk("h", VarId), App(mk("Temp", SenId),[]),
                            PRec(mk("h",RecId), PId(mk("h",RecId))))
        Assert.That( mkDelegate proc
                    , Throws.TypeOf<NameDuplicationException>())

    [<Test>]
    let ``creation of new variable and correct usage then`` () =
        let proc = PAssign( 
                        mk("foo_new", VarId), 
                        App( mk("Temp", SenId), []),
                        PAssign( mk("foo_new", VarId), 
                                    App( mk("+", FuncId),
                                         [
                                            App(mk("5",NumericId), []);
                                            App(mk("foo_new", VarId), [])
                                         ])
                                    , PNil)
                    )

        Assert.AreEqual( procChecker proc,
                        tstore.Add("foo_new", numericResult))


    [<Test>]
    let ``right value as generic try to assign a numeric value, instantiation`` () =
        let proc = PAssign(
                        mk("x", VarId),
                        App( mk("undef2", VarId), []),
                        PNil
            )
        Assert.AreEqual ( procChecker proc,
                         tstore.Add("undef2", numericResult)
                                .Add("CopyUndef2", numericResult)
                        )


    [<Test>]
    let ``right value as generic try to assign another generic value, unification`` () =
        let proc = PAssign(
                        mk("undef1", VarId),
                        App( mk("undef2", VarId), []),
                        PNil
            )
        Assert.AreEqual ( procChecker proc,
                         tstore.Add("undef1", undef2)
                        )

    
    [<Test>]
    let ``right value as generic try to assign another generic value, unification 2`` () =
        let proc = PAssign(
                        mk("undef2", VarId),
                        App( mk("undef1", VarId), []),
                        PNil
            )
        Assert.AreEqual ( procChecker proc,
                         tstore.Add("undef2", undef1)
                                .Add("CopyUndef2", undef1)
                        )
    
        

module PSend =

    [<Test>]
    let ``node-id not existent`` () =
        let proc = PSend([], [ mk("pippo", NodeId)] , PNil )
        Assert.That( mkDelegate proc
                    , Throws.TypeOf<UnexpectedRecipientException>())


    [<Test>]
    let ``rec id as recipient`` () =
        let proc = PSend([], [ mk("i", NodeId)] , PNil )
        Assert.That( mkDelegate proc
                    , Throws.TypeOf<UnexpectedRecipientException>())

    
    [<Test>]
    let ``numeric variable as recipient`` () =
        let proc = PSend([], [ mk("x", NodeId)] , PNil )
        Assert.That( mkDelegate proc
                    , Throws.TypeOf<UnexpectedRecipientException>())


    [<Test>]
    let ``send with a keyword as recipient`` () =
        let proc = PSend([], [ mk("process", NodeId)] , PNil )
        Assert.That( mkDelegate proc
                    , Throws.TypeOf<KeywordDuplicationException>())


    [<Test>]
    let ``send correct, multiple recipient`` () =
        let proc = PSend([App(mk("x",VarId),[]);App(mk("Temp",SenId),[])],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore)


    [<Test>]
    let ``send correct, multiple recipient, expression modify state with inferencing`` () =
        let proc = PSend([
                          App( mk("-", FuncId), 
                            [App(mk("undef2",VarId),[]);App(mk("Temp",SenId),[])]
                          )
                         ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore.Add("undef2", numericResult)
                            .Add("CopyUndef2", numericResult)
        )

    [<Test>]
    let ``send with a double recipient: client, server, client`` () =
        let proc = PSend([
                          App( mk("-", FuncId), 
                            [App(mk("undef2",VarId),[]);App(mk("Temp",SenId),[])]
                          )
                         ],
                         [ mk("Client", NodeId); mk("Server", NodeId); mk("Client", NodeId)] ,
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore.Add("undef2", numericResult)
                            .Add("CopyUndef2", numericResult)
        )


module PSnd =
    
    [<Test>]
    let ``send with a true as label`` () =
        let proc = PSnd(
                         mk(TrueName, FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore
        )
    
    [<Test>]
    let ``send with a false as label`` () =
        let proc = PSnd(
                         mk(FalseName, FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore
        )


    [<Test>]
    let ``send with a green_screen as label`` () =
        let proc = PSnd(
                         mk("green_screen", FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore
        )    

    [<Test>]
    let ``send with 5 as label`` () =
        let proc = PSnd(
                         mk("5", FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore
        )    

    [<Test>]
    let ``send with keyword as label`` () =
        let proc = PSnd(
                         mk("node", FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<KeywordDuplicationException>()
        )

    
    [<Test>]
    let ``exception to use a variable as label`` () =
        let proc = PSnd(
                         mk("x", FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )


    [<Test>]
    let ``exception to use a function as label`` () =
        let proc = PSnd(
                         mk("+", FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )


    [<Test>]
    let ``exception to use an action as label`` () =
        let proc = PSnd(
                         mk("turnon", FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )


    [<Test>]
    let ``exception to use an actuator as label`` () =
        let proc = PSnd(
                         mk("turnonoff", FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )

    [<Test>]
    let ``exception to use an unexistent name`` () =
        let proc = PSnd(
                         mk("foo_boh", FuncId),
                         [ App( mk("x", FuncId), []) ],
                         [ mk("Client", NodeId); mk("Server", NodeId)] ,
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )
       

module PCmd =

    [<Test>]
    let ``correct call`` () =
        let proc = PCmd(mk("Lamp", ActId), mk("turnon", ActionId), PNil)

        Assert.AreEqual(procChecker proc, tstore)


    [<Test>]
    let ``the actuator variable is not an actuator`` () =
        let proc = PCmd(mk("x", ActId), mk("turnon", ActionId), PNil)

        Assert.That(mkDelegate proc, Throws.TypeOf<NameDuplicationException>())


    [<Test>]
    let ``the actuator variable does not exists at all`` () =
        let proc = PCmd(mk("foo_unexistent", ActId), mk("turnoff", ActionId), PNil)

        Assert.That(mkDelegate proc, Throws.TypeOf<NotDeclaredNameException>())

    [<Test>]
    let ``action of a different actuator`` () =
        let proc = PCmd(mk("Lamp", ActId), mk("play", ActionId), PNil)

        Assert.That(mkDelegate proc, Throws.TypeOf<UnexpectedActionException>())

    
    [<Test>]
    let ``action as numeric variable`` () =
        let proc = PCmd(mk("Lamp", ActId), mk("z", ActionId), PNil)

        Assert.That(mkDelegate proc, Throws.TypeOf<UnexpectedActionException>())


    [<Test>]
    let ``tha actuator variable as keyword`` () =
        let proc = PCmd(mk("send", ActId), mk("turnon", ActionId), PNil)

        Assert.That(mkDelegate proc, Throws.TypeOf<KeywordDuplicationException>())


    [<Test>]
    let ``tha action variable as keyword`` () =
        let proc = PCmd(mk("Lamp", ActId), mk("wait_for", ActionId), PNil)

        Assert.That(mkDelegate proc, Throws.TypeOf<KeywordDuplicationException>())


module PCond =

    [<Test>]
    let ``if true then PNil else PNil`` () =
        let proc = PCond(App(mk(TrueName, ActId), []), PNil, PNil)

        Assert.AreEqual(procChecker proc, tstore)

    [<Test>]
    let ``if (x=Temp) then PNil else PNil`` () =
        let proc = PCond(
                    App(mk("=", FuncId), 
                        [
                            App(mk("x", VarId), [])
                            App(mk("Temp", SenId), [])
                        ]),
                    PNil, PNil)

        Assert.AreEqual(procChecker proc, tstore)



    [<Test>]
    let ``guard as numeric`` () =
        let proc = PCond(
                    App(mk("+", ActId), 
                        [
                            App(mk("x", VarId), [])
                            App(mk("Temp", SenId), [])
                        ]),
                    PNil, PNil)
        Assert.That(mkDelegate proc, Throws.TypeOf<NotBooleanConditionException>())


    [<Test>]
    let ``incompatibles branches by mean of assignment`` () =
        let proc = PCond(
                    App(mk("=", FuncId), 
                        [
                            App(mk("x", VarId), [])
                            App(mk("Temp", SenId), [])
                        ]),
                    PAssign(mk("x", VarId), App(mk("5", FuncId), []), PNil),
                    PAssign(mk("x", VarId), App(mk(TrueName, FuncId), []), PNil))
        Assert.That(mkDelegate proc, Throws.TypeOf<UnexpectedAssignmentException>())


    [<Test>]
    let ``error assignment in the first branch`` () =
        let proc = PCond(
                    App(mk("=", FuncId), 
                        [
                            App(mk("x", VarId), [])
                            App(mk("Temp", SenId), [])
                        ]),
                    PAssign(mk("x", VarId), App(mk(TrueName, FuncId), []), PNil),
                    PAssign(mk("x", VarId), App(mk("5", FuncId), []), PNil))
        Assert.That(mkDelegate proc, Throws.TypeOf<UnexpectedAssignmentException>())        


    [<Test>]
    let ``keyword used as guard`` () =
        let proc = PCond(
                    App(mk("tau", ActId), []),
                    PAssign(mk("x", VarId), App(mk(TrueName, FuncId), []), PNil),
                    PAssign(mk("x", VarId), App(mk("5", FuncId), []), PNil))
        Assert.That(mkDelegate proc, Throws.TypeOf<KeywordDuplicationException>()) 


module PReceive = 

    [<Test>]
    let ``variable list with duplicate [x,t,y,x]`` () =
        let proc = PReceive([], [mk("x",VarId);
                                mk("t",VarId);mk("y",VarId);
                                 mk("x",VarId)], PNil )
        
        Assert.That(mkDelegate proc, Throws.TypeOf<DuplicateRecipientException>())


    [<Test>]
    let ``variable list not unique [x,t,y,x]`` () =
        let proc = PReceive([], [mk("x",VarId);
                                mk("t",VarId);mk("y",VarId);
                                 mk("x",VarId)], PNil )
        
        Assert.That(mkDelegate proc, Throws.TypeOf<DuplicateRecipientException>())

    [<Test>]
    let ``within the expressions I can't use the new variables`` () =
        let proc = PReceive([ App(mk("not", FuncId),[App(mk("t", VarId), [])]) ],
                                 [mk("t",VarId)], PNil )
        
        Assert.That(mkDelegate proc, Throws.TypeOf<NotDeclaredNameException>())

    [<Test>]
    let ``correct receive with also infering`` () =
        let proc = PReceive([ App(mk("not", FuncId),[App(mk("undef1", VarId), [])]);
                              App(mk("undef2", FuncId), []) ],
                              [mk("t",VarId)],

                              PAssign(mk("t",VarId), App(mk("x", VarId),[]), PNil) )

        Assert.AreEqual (procChecker proc, tstore.Add("undef1", boolResult)
                                                .Add("t", numericResult)
                        )

    [<Test>]
    let ``correct receive with existing variable`` () =
        let proc = PReceive([ App( mk("Temp", SenId),[] ) ],
                              [mk("x",VarId)],

                              PAssign(mk("y",VarId), App(mk("x", VarId),[]), PNil) )

        Assert.AreEqual (procChecker proc, tstore)


    [<Test>]
    let ``correct receive, try to matching a constant numeric, boolean and a sensor`` () =
        let proc = PReceive([ App( mk("Temp", SenId),[] )
                              App( mk(TrueName, FuncId), [])
                              App( mk("5", NumericId), []) ],
                              [mk("x",VarId)],

                              PAssign(mk("y",VarId), App(mk("x", VarId),[]), PNil) )

        Assert.AreEqual (procChecker proc, tstore)


    [<Test>]
    let ``exception, I can't receive on sensor`` () =
        let proc = PReceive([ App(mk("not", FuncId),[App(mk(FalseName, VarId), [])]) ],
                                 [mk("Temp",SenId)], PNil )
        
        Assert.That(mkDelegate proc, Throws.TypeOf<UnexpectedAssignmentException>())


    [<Test>]
    let ``exception, I can't receive o Rec variable`` () =
        let proc = PReceive([ App(mk("5", NumericId),[]) ],
                                 [mk("i",RecId)], PNil )
        
        Assert.That(mkDelegate proc, Throws.TypeOf<UnexpectedAssignmentException>())



module PRecv =
    
    [<Test>]
    let ``receive with a empty variable list`` () =
        let proc = PRecv(
                         mk("green_screen", FuncId),
                         [ ],
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore
        )


    [<Test>]
    let ``receive with a true as label`` () =
        let proc = PRecv(
                         mk(TrueName, FuncId),
                         [ mk("x", FuncId) ],
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore
        )
    
    [<Test>]
    let ``receive with a false as label`` () =
        let proc = PRecv(
                         mk(FalseName, FuncId),
                         [ mk("x", FuncId) ],
                          PNil )
        Assert.AreEqual( procChecker proc
                    , tstore
        )


    [<Test>]
    let ``receive with a green_screen as label`` () =
        let proc = PRecv(
                         mk("green_screen", FuncId),
                         [ mk("t", VarId) ],

                          PAssign(mk("t",VarId), App(mk("x", VarId),[]), PNil) )
        Assert.AreEqual( procChecker proc
                    , tstore.Add("t", numericResult)
        )    

    [<Test>]
    let ``receive with 5 as label`` () =
        let proc = PRecv(
                         mk("5", NumericId),
                         [ mk("t", VarId) ],
                         
                          PAssign(mk("t",VarId), App(mk("green_screen", VarId),[]), PNil) )
        Assert.AreEqual( procChecker proc
                    , tstore.Add("t",imagetResult)
        )   


    [<Test>]
    let ``receive with keyword as label`` () =
        let proc = PRecv(
                         mk("node", FuncId),
                         [ ],
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<KeywordDuplicationException>()
        )

    
    [<Test>]
    let ``exception to use a variable as label`` () =
        let proc = PRecv(
                         mk("x", FuncId),
                         [  ],
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )


    [<Test>]
    let ``exception to use a function as label`` () =
        let proc = PRecv(
                         mk("+", FuncId),
                         [ ],
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )


    [<Test>]
    let ``exception to use an action as label`` () =
        let proc = PRecv(
                         mk("turnon", FuncId),
                         [  ],
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )


    [<Test>]
    let ``exception to use an actuator as label`` () =
        let proc = PRecv(
                         mk("turnonoff", FuncId),
                         [ ] ,
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )

    [<Test>]
    let ``exception to use an unexistent name`` () =
        let proc = PRecv(
                         mk("foo_boh", FuncId),
                         [ ],
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<NotDeclaredConstantException>()
        )

    [<Test>]
    let ``exception receive on a sensor`` () =
        let proc = PRecv(
                         mk(TrueName, FuncId),
                         [ mk("Temp", SenId) ],
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<UnexpectedAssignmentException>()
        )

    [<Test>]
    let ``exception receive on a constant green_screen`` () =
        let proc = PRecv(
                         mk(FalseName, FuncId),
                         [ mk("green_screen", FuncId) ],
                          PNil )
        Assert.That(
            mkDelegate proc
            , Throws.TypeOf<UnexpectedAssignmentException>()
        )
