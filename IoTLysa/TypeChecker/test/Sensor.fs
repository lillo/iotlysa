namespace TypeChecker.Test.Sensor

open FParsec
open NUnit.Framework
open SymbolTable
open LysaParser
open ParseTree
open TypeChecker

[<AutoOpenAttribute>]
module Utils =
    let mk = makeSymbol

    let (sensorChecker, tstore) = 
        let text = 
            "
                type image_t
                func resizeImage(image_t) : image_t
                func green_screen : image_t
                actuator_type turnonoff {turnon, turnoff}
                actuator_type stereo { play, stop}
            "
        let symTable = SymbolTable()
        let lysaParser = LysaParser(symTable)
        ignore ( symTable.AddSymbol "5" NumericId)
        match lysaParser.ParseOnString text with
            | Success((sigs, ndecls),_,_) ->
                let tenv = Algorithms.SigDelaration.Checker (makeTEnvStart(symTable)) sigs
                let gstate = GlobalState tenv ndecls

                let getActuator actuName =
                    match tenv.[actuName] with
                    | TEImActuator t -> t
                    | _ -> failwith "internal error taking the actuator value."

                let evalState = 
                    [   
                        "h", REC
                        "Temp", Sensor( NumericType)
                        "Lamp", TSImActuator( getActuator "turnonoff" )
                        "Cassa", TSImActuator( getActuator "stereo")
                    ] |>
                    List.fold (fun (s:TStore) p -> s.Add p) gstate
                    

                ((Algorithms.Sensor.Checker tenv evalState), evalState)
            | Failure( _ ) -> failwith "error during the parsing."

    let mkDelegate expr = (fun () -> ignore <| sensorChecker expr)

module SNilTauProbe =

    [<Test>]
    let ``SNil case`` () =
        let tree = SNil
        Assert.AreEqual(sensorChecker tree, tstore)

    [<Test>]
    let ``STau.SNil case`` () =
        let tree = STau(SNil)
        Assert.AreEqual(sensorChecker tree, tstore)

    [<Test>]
    let ``SProbe.STau.SNil case`` () =
        let tree = SProbe(STau(SNil))
        Assert.AreEqual(sensorChecker tree, tstore)


    [<Test>]
    let ``STau.SProbe.SNil case`` () =
        let tree = STau(SProbe(SNil))
        Assert.AreEqual(sensorChecker tree, tstore)


module SRec =

    [<Test>]
    let ``Correct rec i; probe; tau; i`` () =
        let name = mk("i", RecId)
        let tree = SRec( name , SProbe( STau( SId( name ) ) ) )
        Assert.AreEqual( sensorChecker tree, tstore.Add("i", REC) )

    [<Test>]
    let ``SRec with id as keyword`` () =
        let name = mk("switch", RecId)
        let tree = SRec( name , STau( SProbe( SId( name ) ) ) )
        Assert.That( mkDelegate tree, Throws.TypeOf<KeywordDuplicationException>() )

    [<Test>]
    let ``SRec with as a name already in the store`` () =
        let name = mk("Temp", RecId)
        let tree = SRec( name , STau( SProbe( SId( name ) ) ) )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SRec with as a name already in the store 2`` () =
        let name = mk("Lamp", RecId)
        let tree = SRec( name , STau( SProbe( SId( name ) ) ) )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SRec with as a name already in the store 3`` () =
        let name = mk("Cassa", RecId)
        let tree = SRec( name , STau( SProbe( SId( name ) ) ) )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SRec with as an existent actuator`` () =
        let name = mk("turnonoff", RecId)
        let tree = SRec( name , STau( SProbe( SId( name ) ) ) )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SRec with as an existent action`` () =
        let name = mk("turnoff", RecId)
        let tree = SRec( name , STau( SProbe( SId( name ) ) ) )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SRec with as an existent type`` () =
        let name = mk( "image_t", RecId)
        let tree = SRec( name , STau( SProbe( SId( name ) ) ) )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SRec with as an existent function`` () =
        let name = mk("green_screen", RecId)
        let tree = SRec( name , STau( SProbe( SId( name ) ) ) )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SRec inside SRec with the same name id`` () =
        let name = mk("i", RecId)
        let tree = SRec( name , SRec( name, STau( SProbe( SId( name ) ) ) ) )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )


module SId =

    [<Test>]
    let ``SId with a keyword`` () =
        let name = mk("else", RecId)
        let tree = SId( name )
        Assert.That( mkDelegate tree, Throws.TypeOf<KeywordDuplicationException>() )

    [<Test>]
    let ``SId with a rec variable in the store`` () =
        let name = mk("h", RecId)
        let tree = SId( name )
        Assert.AreEqual( sensorChecker tree, tstore )

    [<Test>]
    let ``SId with an unexistent name`` () =
        let name = mk("i", RecId)
        let tree = SId( name )
        Assert.That( mkDelegate tree, Throws.TypeOf<NotDeclaredNameException>() )

    [<Test>]
    let ``SId with a name altready in the store not REC`` () =
        let name = mk("Temp", RecId)
        let tree = SId( name )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SId with a name altready in the store not REC 2`` () =
        let name = mk("Lamp", RecId)
        let tree = SId( name )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SId with a name altready in the store not REC 3`` () =
        let name = mk("Cassa", RecId)
        let tree = SId( name )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SId with a name as a actuator`` () =
        let name = mk("Lamp", RecId)
        let tree = SId( name )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SId with a name as an action`` () =
        let name = mk("turnon", RecId)
        let tree = SId( name )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SId with a name as a type`` () =
        let name = mk("image_t", RecId)
        let tree = SId( name )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )

    [<Test>]
    let ``SId with a name as a function`` () =
        let name = mk("resizeImage", RecId)
        let tree = SId( name )
        Assert.That( mkDelegate tree, Throws.TypeOf<NameDuplicationException>() )
