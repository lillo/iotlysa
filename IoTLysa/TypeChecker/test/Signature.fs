namespace TypeChecker.Test.Signature

    open NUnit.Framework
    open FParsec
    open SymbolTable
    open LysaParser
    open TypeChecker

    [<AutoOpenAttribute>]
    module private Util =
        let imageT = Type("image_t")
        let TEnvStart = makeTEnvStart (SymbolTable())

        let evalSig text = 
            let symTable = SymbolTable()
            let lysaParser = LysaParser(symTable)
            match lysaParser.ParseSignaturesOnString text with
                | Success(sigs,_,_) ->
                    Algorithms.SigDelaration.Checker (makeTEnvStart(symTable)) sigs
                | Failure( _ ) -> failwith "unexpected error during the parsing."

        let mkDelegate text = fun () -> evalSig text |> ignore

    module Foo =

        [<Test>]
        let ``empty program`` () = 
            Assert.AreEqual(
                (evalSig ""), TEnvStart
            )


        [<Test>]
        let ``simple program`` () = 
            let actu = Actuator("turnonoff", set [Action "turnon"; Action "turnoff"])
            Assert.AreEqual(
                evalSig ( sprintf "type image_t
                         func alterate(image_t, %s) : image_t
                         actuator_type turnonoff {turnon, turnoff}" NumericName ),
                TEnvStart
                        .Add("image_t", TEImType( imageT ) )
                        .Add("alterate", TEImFunc( Func("alterate",
                                                        [imageT; NumericType]
                                                        , imageT ) ))
                        .Add("turnonoff", TEImActuator(actu ))
                        .Add("turnon", TEImAction actu)
                        .Add("turnoff", TEImAction actu)
            )


        [<Test>]
        let ``keyword duplication`` () =
            Assert.That( 
                mkDelegate  ("type " + NumericName)
                ,Throws.TypeOf<KeywordDuplicationException>()
            )


        [<Test>]
        let ``name duplication`` () =
            Assert.That (
                mkDelegate <| "type image_t 
                                     func foo: "+BoolName+"
                                     type image_t"
                , Throws.TypeOf<NameDuplicationException>()                                     
            )
        
        [<Test>]
        let ``type not declared`` () =
            Assert.That (
                mkDelegate <| "func foo(image_t) : "+NumericName+"
                                                     type image_t"
                , Throws.TypeOf<TypeNotDeclaredException>()                                     
            )

        [<Test>]
        let ``double action`` () =
            Assert.That (
                mkDelegate "actuator_type ttt { a, b, c, a}"
                , Throws.TypeOf<DoubleActionException>()                                     
            )


    module TypeDecl =

        module AllOk = 
            [<Test>]
            let ``single declaration`` () = 
                Assert.AreEqual( 
                    evalSig "type image_t"
                 , TEnvStart.Add("image_t", TEImType imageT)  )


        module KeywordDuplication = 
            [<Test>]
            let ``keyword duplication: process`` () = 
                Assert.That( 
                    mkDelegate  "type process"
                    ,Throws.TypeOf<KeywordDuplicationException>()
                )

            [<Test>]
            let ``keyword duplication: numeric`` () = 
                Assert.That( 
                    mkDelegate  <| "type "+NumericName
                    ,Throws.TypeOf<KeywordDuplicationException>()
                )


        module NameDuplication = 
            [<Test>]
            let ``double declaration`` () =
                Assert.That( 
                    mkDelegate <| "type image_t
                     func foo: "+NumericName+" 
                     type image_t"
                    ,Throws.TypeOf<NameDuplicationException>()
                )

     
            [<Test>]
            let ``as func already declared 1`` () =
                Assert.That( 
                    mkDelegate  <| "func foo:  "+BoolName+"
                     type foo"
                    ,Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``as func already declared 2`` () =
                Assert.That( 
                    mkDelegate <| 
                        sprintf "func foo(%s, %s) : %s type foo" NumericName BoolName BoolName
                    ,Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``as actuator name`` () =
                Assert.That( 
                    mkDelegate  "actuator_type foo {op1, op2}
                    type foo"
                    ,Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``as action name 1`` () =
                Assert.That( 
                    mkDelegate  "actuator_type foo {op1, op2}
                    type op1"
                    ,Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``as action name 2`` () =
                Assert.That( 
                    mkDelegate  "actuator_type foo {op1, op2}
                    type op2"
                    ,Throws.TypeOf<NameDuplicationException>()
                )


    module FuncDecl =

        module AllOk =
            [<Test>]
            let ``declaration as constant`` () =
                Assert.AreEqual( evalSig ("func foo : "+BoolName),
                    TEnvStart.Add("foo", TEImFunc( Func("foo", [], BoolType) ) ) )

            [<Test>]
            let ``declaration with two arguments`` () =
                Assert.AreEqual( evalSig 
                        (sprintf"func areEqual(%s, %s) : %s" NumericName NumericName BoolName ),
                    TEnvStart.Add("areEqual", TEImFunc( Func("areEqual", [NumericType; NumericType], BoolType) ) ) )

            [<Test>]
            let ``declaration with user-type as argument 1`` () =
                Assert.AreEqual( evalSig <| "type image_t 
                                            func foo(image_t) : "+NumericName
                    , TEnvStart.Add("image_t", TEImType(imageT))
                                .Add("foo", TEImFunc( Func("foo", [imageT], NumericType) ) )
                )

            [<Test>]
            let ``declaration with user-type as argument 2`` () =
                Assert.AreEqual( evalSig 
                        (sprintf "type image_t func foo(%s, image_t) : %s" NumericName NumericName)
                    , TEnvStart.Add("image_t", TEImType(imageT))
                                .Add("foo", TEImFunc( Func("foo", [NumericType; imageT], NumericType) ) )
                )

            [<Test>]
            let ``declaration with user-type as argument 3`` () =
                Assert.AreEqual( evalSig 
                    (sprintf "type image_t func foo(%s ,image_t  ,  %s) : %s" NumericName BoolName NumericName )
                    , TEnvStart.Add("image_t", TEImType(imageT))
                                .Add("foo", TEImFunc( Func("foo", [NumericType; imageT; BoolType], NumericType) ) )
                )

            [<Test>]
            let ``declaration with user-type as return type`` () =
                Assert.AreEqual( 
                       evalSig (sprintf "type image_t func foo(%s, %s) : image_t" BoolName NumericName )
                    , TEnvStart.Add("image_t", TEImType(imageT))
                                .Add("foo", TEImFunc( Func("foo", [BoolType; NumericType], imageT) ) )
                )

        module KeywordDuplication = 
            [<Test>]
            let ``keyword 'numeric' as func name`` () =
                Assert.That(
                    mkDelegate (sprintf "func %s : %s" NumericName BoolName),
                    Throws.TypeOf<KeywordDuplicationException>()
                )

            [<Test>]
            let ``keyword 'node' as func name`` () =
                Assert.That(
                    mkDelegate <| "func node : "+BoolName,
                    Throws.TypeOf<KeywordDuplicationException>()
                )

        module TypeNotDeclared =
            [<Test>]
            let ``keyword 'node' as return type`` () =
                Assert.That(
                    mkDelegate "func foo : node",
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``type not declared as return type`` () =
                Assert.That(
                    mkDelegate <| (sprintf "func foo(%s): image_t" NumericName),
                    Throws.TypeOf<TypeNotDeclaredException>()
                )            

            [<Test>]
            let ``type not declared as constant`` () =
                Assert.That(
                    mkDelegate "func foo: image_t",
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``type not declared as argument 1`` () =
                Assert.That(
                    mkDelegate <| "func foo(image_t): "+BoolName,
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``type not declared as argument 2`` () =
                Assert.That(
                    mkDelegate (sprintf "func foo(%s, image_t): %s" NumericName BoolName),
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``type not declared as argument 3`` () =
                Assert.That(
                    mkDelegate (sprintf "func foo(%s, %s, image_t): %s" NumericName BoolName BoolName),
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``type declared after the func declaration`` () =
                Assert.That(
                    mkDelegate "func foo: image_t
                                type image_t",
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``constant 'true' as return type`` () =
                Assert.That(
                    mkDelegate <| "func foo : "+TrueName,
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``constant 'true' as argument type`` () =
                Assert.That(
                    mkDelegate (sprintf "func foo(%s) : %s" TrueName BoolName),
                    Throws.TypeOf<TypeNotDeclaredException>()
                )        

            [<Test>]
            let ``argument as actuator name`` () =
                Assert.That(
                    mkDelegate <| "actuator_type t {op1,op2}
                                  func foo(t) : "+NumericName,
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``argument as action name 1`` () =
                Assert.That(
                    mkDelegate <| "actuator_type t {op1,op2}
                                func foo(op1) : "+NumericName,
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``argument as action name 2`` () =
                Assert.That(
                    mkDelegate <| "actuator_type t {op1,op2}
                                 func foo(op2) : "+NumericName,
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``return type as actuator name`` () =
                Assert.That(
                    mkDelegate (sprintf "actuator_type t {op1,op2}
                                func foo(%s) : t" NumericName),
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``return type as action name 1`` () =
                Assert.That(
                    mkDelegate (sprintf "actuator_type t {op1,op2}
                                func foo(%s) : op1" NumericName),
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

            [<Test>]
            let ``return type as action name 2`` () =
                Assert.That(
                    mkDelegate (sprintf "actuator_type t {op1,op2}
                                func foo(%s) : op2" NumericName),
                    Throws.TypeOf<TypeNotDeclaredException>()
                )

        module NameDuplication =

            [<Test>]
            let ``actuator name duplication`` () =
                Assert.That(
                    mkDelegate <| "actuator_type t {op1, op2} 
                                func t : "+BoolName,
                    Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``action name duplication 1`` () =
                Assert.That(
                    mkDelegate <| "actuator_type t {op1, op2} 
                                func op1 : "+BoolName,
                    Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``action name duplication 2`` () =
                Assert.That(
                    mkDelegate <| "actuator_type t {op1, op2} 
                                func op2 : "+BoolName,
                    Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``type name duplication`` () =
                Assert.That(
                    mkDelegate <| "type image_t 
                                func image_t : "+BoolName,
                    Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``func name duplication`` () =
                Assert.That(
                    mkDelegate (sprintf "func foo : %s 
                                         func foo : %s" BoolName BoolName),
                    Throws.TypeOf<NameDuplicationException>()
                )

    module ActuatorDecl =

        module AllOk =
        
            [<Test>]
            let ``no actions`` () =
                Assert.AreEqual(
                    evalSig "actuator_type t {}"
                    , TEnvStart.Add( "t", TEImActuator( Actuator("t", set []) ) )
                )

            [<Test>]
            let ``two actuators without any clash`` () =
                let fooActu = Actuator("foo", set [Action "a"; Action "b"; Action "c"])
                let tActu = Actuator("t", set [Action "op"])
                Assert.AreEqual(
                    evalSig "actuator_type foo{ a, b,c} actuator_type t {op}"
                    , TEnvStart.Add( "t", TEImActuator( tActu ) )
                                .Add( "foo", TEImActuator( fooActu ))
                                .Add( "a", TEImAction( fooActu ))
                                .Add( "b", TEImAction( fooActu ))
                                .Add( "c", TEImAction( fooActu ))
                                .Add( "op", TEImAction( tActu ))
                )


        module KeywordDuplication =

            [<Test>]
            let ``keyword 'numeric' as principal name`` () =
                Assert.That(
                    mkDelegate (sprintf "actuator_type %s { }" NumericName)
                    , Throws.TypeOf<KeywordDuplicationException>()
                )

            [<Test>]
            let ``keyword 'process' as principal name`` () =
                Assert.That(
                    mkDelegate "actuator_type process { op1}"
                    , Throws.TypeOf<KeywordDuplicationException>()
                )

            [<Test>]
            let ``keyword 'func' as principal name`` () =
                Assert.That(
                    mkDelegate "actuator_type func { }"
                    , Throws.TypeOf<KeywordDuplicationException>()
                )

            [<Test>]
            let ``keyword 'true' as principal name`` () =
                Assert.That(
                    mkDelegate (sprintf "actuator_type %s { op1}" TrueName)
                    , Throws.TypeOf<KeywordDuplicationException>()
                )

            [<Test>]
            let ``keyword 'numeric' as principal action 1`` () =
                Assert.That(
                    mkDelegate (sprintf "actuator_type foo { %s, a}" NumericName)
                    , Throws.TypeOf<KeywordDuplicationException>()
                )

            [<Test>]
            let ``keyword 'process' as principal action 2`` () =
                Assert.That(
                    mkDelegate "actuator_type foo { a, process}"
                    , Throws.TypeOf<KeywordDuplicationException>()
                )

        
        module NameDuplication =

            [<Test>]
            let ``same name as type declared`` () =
                Assert.That(
                    mkDelegate "type image_t
                                actuator_type image_t {}"
                    , Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``same name as func declared`` () =
                Assert.That(
                    mkDelegate (sprintf "func foo : %s 
                                                      actuator_type foo {}" NumericName)
                    , Throws.TypeOf<NameDuplicationException>()
                )


            [<Test>]
            let ``same name as actuator declared`` () =
                Assert.That(
                    mkDelegate "actuator_type act1 {op1, op2}
                                                    actuator_type act1{}"
                    , Throws.TypeOf<NameDuplicationException>()
                )


            [<Test>]
            let ``same name as action declared 1`` () =
                Assert.That(
                    mkDelegate "actuator_type act1 {op1, op2}
                                                    actuator_type op1{}"
                    , Throws.TypeOf<NameDuplicationException>()
                )


            [<Test>]
            let ``same name as action declared 2`` () =
                Assert.That(
                    mkDelegate "actuator_type act1 {op1, op2}
                                                    actuator_type op2{}"
                    , Throws.TypeOf<NameDuplicationException>()
                )


            [<Test>]
            let ``same action as a type declared`` () =
                Assert.That(
                    mkDelegate "type image_t
                                                     actuator_type foo{ op1, image_t, op2}"
                    , Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``same action as a func declared`` () =
                Assert.That(
                    mkDelegate (sprintf "func image_t : %s
                                actuator_type foo{ op1, image_t, op2}" NumericName)
                    , Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``same action as an actuator declared`` () =
                Assert.That(
                    mkDelegate "actuator_type ttt {}
                                                     actuator_type foo{ op1, ttt, op2}"
                    , Throws.TypeOf<NameDuplicationException>()
                )

            [<Test>]
            let ``same action as an action declared in another actuator`` () =
                Assert.That(
                    mkDelegate "actuator_type ferfe { ttt }
                                                     actuator_type foo{ op1, ttt, op2}"
                    , Throws.TypeOf<NameDuplicationException>()
                )

        module DoubleAction =

            [<Test>]
            let ``case: a, b, c, a`` () =
                Assert.That(
                    mkDelegate "actuator_type ttt { a, b, c, a}"
                    , Throws.TypeOf<DoubleActionException>()
                )

            [<Test>]
            let ``case: c, a, c, b`` () =
                Assert.That(
                    mkDelegate "actuator_type ttt { c, a, c, b}"
                    , Throws.TypeOf<DoubleActionException>()
                )
