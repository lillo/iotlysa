# README #

This is a compiler from IoTLySa specification to CFSMs. 

This project is a proof of concept in a very early stages (see below for TODOs and known issues) implementing the translation described in **Tool Supported Analysis of IoT** (C. Bodei, P. Degano, L. Galletta, and E. Tuosto, ICE 2017). 


## Requirements to build the project #

The project requires 

- Microsoft .NET Framework SDK or Mono

- FSharp compiler

- [NuGet](https://www.nuget.org/) package manager

- [FParsec](http://www.quanttec.com/fparsec/) combinator library. 
  The library can be installed using [NuGet](https://www.nuget.org/) package manager



## Building the project #

To download and install FParsec use nuget 
```
$ nuget restore IoTLysa.sln 
```

To build on Windows use Visual Studio or msbuild by the command line:
```
 > msbuild IoTLysa.sln
```


To build on Mono use MonoDevelop or xbuild by the command line:

```
 $ xbuild IoTLysa.sln

```

## Running #

Usage:

```
 $ IoTLysa.exe <lysa specification> <compatibility> 

```

where the first argument is a .lysa file containing the system, the other one is a .comp file specifying the communication range of nodes.
The tool results in some debug prints and produces two output file: a .fsa file to use with Chorgram and a .debug file containing other debug information.

### Examples
The directory IoTLysa/Sample contains some IoTLysa specification used for testing, whereas IoTLysa/Sample/icepaper17example contains 
the case study of the paper:

- average-new-comm-no-send.lysa -> the system with communication problems;
- average-new-comm.lysa -> the correct system
- average-new-comm.comp -> the specification of communication range.

In the directory bin/Debug to execute the tool run

```
 $ IoTLysa.exe ../../Sample/icepaper17example/average-new-comm-no-send.lysa ../../Sample/icepaper17example/average-new-comm.comp
 $ IoTLysa.exe ../../Sample/icepaper17example/average-new-comm.lysa ../../Sample/icepaper17example/average-new-comm.comp

```

Use chorgram on the resulting .fsa files.


## Known issues (to solve in future versions)

- Sometimes on Windows the tool generates  .fsa and .debug files with an empty name (only the extension).

### TODOS #

- Type checking IoTLysa specification
- Check unbound identifiers
- Improve the termination of the tool when a error during parsing occurs


## Content of the archive #

- README.md                                  this file
- IoTLysa.sln                                Visual Studio (MonoDevelop) solution
- IoTLysa/                                   directory of sources files
- IoTLysa/Sample                             examples of specification in IoTLysa
- IoTLysa/Sample/icepaper17example           system presented in the paper
- IoTLysa/Sample/icetalk17example            system presented during the talk